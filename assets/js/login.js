// Login 
$(document).ready(function () {
	$('#logForm').submit(function (e) {
		e.preventDefault();
		$('#logText').html('<i class="mdi mdi-loading mdi-spin"></i>&nbsp; Checking...');
		var user = $('#logForm').serialize();
		var login = function () {
			$.ajax({
				type: 'POST',
				url: url + '/login/do_login',
				dataType: 'json',
				data: user,
				success: function (response) {
					$('#message').html(response.message);
					$('#logText').html('Login');
					if (response.error) {
						$('#responseDiv').removeClass('alert alert-success').addClass('alert alert-danger').show();
						$('#email').addClass("is-invalid");
						$('#password').addClass("is-invalid");
					} else {
						$('#responseDiv').removeClass('alert alert-danger').addClass('alert alert-success').show();
						// $('#logForm')[0].reset();
						$('#email').removeClass("is-invalid");
						$('#password').removeClass("is-invalid");
						$('#email').addClass("is-valid");
						$('#password').addClass("is-valid");
						$('.toggle-password').hide();
						setTimeout(function () {
							location.reload();
						}, 3000);
					}
				}
			});
		};
		setTimeout(login, 3000);
	});
})

// validasi hanya angka
function hanyaAngka(evt) {
	var theEvent = evt || window.event;

	// Handle paste
	if (theEvent.type === 'paste') {
		key = event.clipboardData.getData('text/plain');
	} else {
		// Handle key press
		var key = theEvent.keyCode || theEvent.which;
		key = String.fromCharCode(key);
	}
	var regex = /[0-9]|\./;
	if (!regex.test(key)) {
		theEvent.returnValue = false;
		if (theEvent.preventDefault) theEvent.preventDefault();
	}
}

// konvert ke rupiah
function convertToRupiah(angka) {
	var rupiah = '';
	var angkarev = angka.toString().split('').reverse().join('');
	for (var i = 0; i < angkarev.length; i++)
		if (i % 3 == 0) rupiah += angkarev.substr(i, 3) + '.';
	return 'Rp. ' + rupiah.split('', rupiah.length - 1).reverse().join('');
}

// $("#showAll").dataTable({
	// lengthMenu: [
	// 	["All"]
	// ],
	// responsive: !0
// })