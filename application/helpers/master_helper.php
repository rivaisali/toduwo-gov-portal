<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
// function __construct() {
// 	parent::__construct();
// 	$this->load->model("crud_model");
// }

// ============================================
// ambil data return result
// ============================================
function ambil_data($tabel, $where = array())
{
	$CI = &get_instance();
	$CI->db->select('*');
	$CI->db->where($where);
	$CI->db->from($tabel);
	$query 	= 	$CI->db->get();
	$data	=	$query->result();
	return $data;
}

// ============================================
// ambil nama dari table
// ============================================
function ambil_nama_by_id($table, $field, $field_by, $key)
{
	$CI = &get_instance();
	$sql	=	$CI->crud_model->select_one($table, $field_by, $key);
	$data	= 	empty($sql) ? "-" : $sql->$field;
	return $data;
}

// ============================================
// ambil data by id return result
// ============================================
function ambil_data_by_id($tabel, $field, $id)
{
	$CI = &get_instance();
	$CI->db->select('*');
	$CI->db->where($field, $id);
	$CI->db->from($tabel);
	$query 	= 	$CI->db->get();
	$data	=	$query->result();
	return $data;
}

// ============================================
// ambil data by id return row
// ============================================
function ambil_data_by_id_row($tabel, $field, $id)
{
	$CI = &get_instance();
	$CI->db->select('*');
	$CI->db->where($field, $id);
	$CI->db->from($tabel);
	$query 	= 	$CI->db->get();
	$data	=	$query->row();
	return $data;
}

// ============================================
// ambil data as sum by id return row
// ============================================
function ambil_data_sum_by_id_row($tabel, $field, $where, $join = null)
{
	$CI = &get_instance();
	$CI->db->select_sum($field);
	$CI->db->where($where);
	$CI->db->from($tabel);
	if ($join !== null) {
		$CI->db->join($join["table"], $tabel . "." . $join["key"] . " = " . $join["table"] . "." . $join["key"]);
	}
	$query 	= 	$CI->db->get();
	$data	=	$query->row();
	return $data;
}

// ============================================
// ambil data beberapa field by id where array return row
// ============================================
function ambil_datafield_by_id_row($tabel, $field, $where)
{
	$CI = &get_instance();
	$CI->db->select($field);
	$CI->db->where($where);
	$CI->db->from($tabel);
	$query 	= 	$CI->db->get();
	$data	=	$query->row();
	return $data;
}

// ============================================
// ambil data by id return row where array
// ============================================
function ambil_data_by_id_row_where_array($tabel, $where)
{
	$CI = &get_instance();
	$CI->db->select('*');
	$CI->db->where($where);
	$CI->db->from($tabel);
	$query 	= 	$CI->db->get();
	$data	=	$query->row();
	return $data;
}

// ============================================
// ambil data by id return result where array
// ============================================
function ambil_data_by_id_where_array($tabel, $where)
{
	$CI = &get_instance();
	$CI->db->select('*');
	$CI->db->where($where);
	$CI->db->from($tabel);
	$query 	= 	$CI->db->get();
	$data	=	$query->result();
	return $data;
}

// ============================================
// ambil num_row by id return num_row where
// ============================================
function ambil_numrow_by_id($tabel, $field, $id)
{
	$CI = &get_instance();
	$CI->db->select('*');
	$CI->db->where($field, $id);
	$CI->db->from($tabel);
	$query 	= 	$CI->db->get();
	$data	=	$query->num_rows();
	return $data;
}

// ============================================
// ambil num_row by id return num_row where array
// ============================================
function ambil_numrow_by_id_where_array($tabel, $where)
{
	$CI = &get_instance();
	$CI->db->select('*');
	$CI->db->where($where);
	$CI->db->from($tabel);
	$query 	= 	$CI->db->get();
	$data	=	$query->num_rows();
	return $data;
}

// ============================================
// cek data sudah ada atau belum // jika sudah return true
// ============================================
function cek_data_existing($tabel, $where = array())
{
	$CI = &get_instance();
	$CI->db->select('*');
	$CI->db->where($where);
	$CI->db->from($tabel);
	$query 	= 	$CI->db->get();
	$data	=	$query->row();
	if (!empty($data)) {
		return true;
	} else {
		return false;
	}
}
