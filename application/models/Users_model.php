<?php
class Users_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	public function login($email)
	{
		// $query = $this->db->get_where('tb_user', array('username'=>$username, 'password'=>$password));
		$query = $this->db->select('*')
			->where('email', $email)
			// ->where('password', $password)
			->get('user');
		return $query->row_array();
	}
}
