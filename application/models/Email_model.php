<?php
class Email_model extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function kirim_email($tujuan, $judul, $content)
	{
		$pesan = '
			<!DOCTYPE html>
			<html>

			<head>
				<meta charset="utf-8">
				<meta http-equiv="X-UA-Compatible" content="IE=edge">
				<title>SIPARDI</title>
				<!-- Tell the browser to be responsive to screen width -->
				<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
				<!-- Google Font -->
				<link href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" rel="stylesheet">
				<link href="' . base_url("assets/css/email.css") . '" rel="stylesheet">
			</head>

			<body>
				<header>
					<img src="' . base_url() . 'assets/images/logo-top.png" alt="Logo Sipardi">
				</header>
				<main>
					<div class="title">' . $judul . '</div>
					<div class="content">
						' . $content . '
					</div>
					<footer>
						<div class="social-media">
							<a href=""><img src="' . base_url("assets/images/facebook.png") . '"></a>							
							<a href=""><img src="' . base_url("assets/images/instagram.png") . '"></a>							
							<a href=""><img src="' . base_url("assets/images/whatsapp.png") . '"></a>							
						</div>
					</footer>
				</main>
			</body>

			</html>';
		// $this->load->library('email');
		// $this->email->from("hello@toduwo.id", "Toduwo.id");
		// $this->email->to($tujuan);
		// $this->email->subject($judul);
		// $this->email->message($pesan);
		// $this->email->message($content);
		return $pesan;
		// return $this->email->send();
		// if (!$this->email->send()) {
		// 	show_error($this->email->print_debugger());
		// } else {
		// 	echo 'Success to send email';
		// }
	}
}
