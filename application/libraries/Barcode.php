<?php

use Endroid\QrCode\Color\Color;
use Endroid\QrCode\Encoding\Encoding;
use Endroid\QrCode\ErrorCorrectionLevel\ErrorCorrectionLevelLow;
use Endroid\QrCode\Label\Label;
use Endroid\QrCode\Logo\Logo;
use Endroid\QrCode\QrCode;
use Endroid\QrCode\RoundBlockSizeMode\RoundBlockSizeModeMargin;
use Endroid\QrCode\Writer\PngWriter;

class Barcode
{
    public function __construct()
    {
        $this->CI = &get_instance();
        require_once APPPATH . '/third_party/qrcode/autoload.php';
    }

    public function createQrCode($data)
    {
        $writer = new PngWriter();

        // Create QR code
        $qrCode = QrCode::create($data)
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(300)
            ->setMargin(10)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        // Create generic logo
        $logo = Logo::create('uploads/qrcode/logo-toduwo.png')
            ->setResizeToWidth(100);

        // Create generic label
        $label = Label::create('toduwo_gov')
            ->setTextColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        $result = $writer->write($qrCode);
        // Save it to a file
        $result->saveToFile('uploads/qrcode/' . $data . '.png');
    }

    // create qrcode undangan
    public function createQrCodeUndangan($data, $nama)
    {
        $writer = new PngWriter();

        // Create QR code
        $qrCode = QrCode::create($data)
            ->setEncoding(new Encoding('UTF-8'))
            ->setErrorCorrectionLevel(new ErrorCorrectionLevelLow())
            ->setSize(300)
            ->setMargin(10)
            ->setRoundBlockSizeMode(new RoundBlockSizeModeMargin())
            ->setForegroundColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        // Create generic logo
        $logo = Logo::create('uploads/qrcode/logo-toduwo.png')
            ->setResizeToWidth(100);

        // Create generic label
        $label = Label::create('toduwo_gov')
            ->setTextColor(new Color(0, 0, 0))
            ->setBackgroundColor(new Color(255, 255, 255));

        $result = $writer->write($qrCode);
        // Save it to a file
        $result->saveToFile('uploads/qrcode/' . $nama . '.png');
    }
}
