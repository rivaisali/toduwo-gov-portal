<?php

class Phpspreadsheet
{

    public function __construct()
    {
        $this->CI = &get_instance();
        require_once APPPATH . '/third_party/phpspreadsheet/autoload.php';
    }

    public function import($inputFileName)
    {
        $inputFileType = 'Xlsx';
        $reader = IOFactory::createReader($inputFileType);
        $spreadsheet = $reader->load($inputFileName);

// Read an array list of any custom properties for this document
        $customPropertyList = $spreadsheet->getProperties()->getCustomProperties();

        foreach ($customPropertyList as $customPropertyName) {
            echo print_r($customPropertyName);
        }

    }

}
