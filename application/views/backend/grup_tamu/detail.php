<div class="row">
    <div class="col-sm-12">
        <div class="user-info-list card">
            <div class="card-header card-header-divider">
                Data Grup
                <div class="tools dropdown">
                    <a href="<?= base_url($base); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-arrow-back text-white"></span> Kembali
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <table class="no-border no-strip skills">
                            <tbody class="no-border-x no-border-y">
                                <tr>
                                    <td class="icon"><span class="mdi mdi-badge-check"></span></td>
                                    <td class="item">Nama Grup<span class="icon s7-portfolio"></span></td>
                                    <td><?= $data->nama_grup_tamu; ?></td>
                                </tr>
                                <tr>
                                    <td class="icon"><span class="mdi mdi-storage"></span></td>
                                    <td class="item">Deskripsi<span class="icon s7-gift"></span></td>
                                    <td><?= $data->deskripsi; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">
                Daftar Tamu
                <div class="tools dropdown">
                    <a href="<?= base_url($base . "/tamu/" . $data->grup_tamu_id); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-account-add text-white"></span> Tambah Tamu Ke Dalam Grup
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($tamu as $t) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $t->nama_lengkap; ?></td>
                                <td><?= $t->alamat; ?></td>
                                <td>
                                    <a title="Hapus" class="icon mx-1 delete" href="<?= base_url($base . "/hapus_tamu/" . $t->grup_tamu_id . "/" . $t->grup_tamu_detail_id); ?>" id="hapusData">
                                        <i class="mdi mdi-delete text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>