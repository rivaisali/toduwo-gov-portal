<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header card-header-divider">
                Tambah Tamu
                <div class="tools dropdown">
                    <a href="<?= base_url($base . "/detail/" . $data->grup_tamu_id); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-arrow-back text-white"></span> Kembali
                    </a>
                </div>
            </div>
            <div class="card-body">
                <?= form_open(""); ?>
                <?= form_hidden('grup_tamu_id', $data->grup_tamu_id); ?>
                <?php
                if (form_error('tamu[]')) :
                    echo "<div class='alert alert-danger alert-dismissible mt-2' role='alert'>
                    <button class='close' type='button' data-dismiss='alert' aria-label='Close'><span class='mdi mdi-close' aria-hidden='true'></span></button>
                    <div class='icon'><span class='mdi mdi-close-circle-o'></span></div>
                    <div class='message'><strong>Error!</strong> " . form_error('tamu[]', "<span>", "</span>") . "</div>
                  </div>";
                endif;
                ?>
                <table class="table table-striped table-hover table-fw-widget bg-white" id="showall">
                    <thead>
                        <tr>
                            <th>
                                <div class="be-checkbox custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="checkAll">
                                    <label class="custom-control-label ml-6" for="checkAll">Pilih Semua</label>
                                </div>
                            </th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Instansi</th>
                            <th>Jabatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($tamu as $t) : ?>
                            <tr>
                                <td>
                                    <div class="be-checkbox custom-control custom-checkbox">
                                        <input name="tamu[<?= $no; ?>]" value="<?= $t->tamu_id; ?>" class="custom-control-input" type="checkbox" id="check<?= $no; ?>" <?= set_checkbox("tamu[$no]", $t->tamu_id); ?>>
                                        <label class="custom-control-label ml-6" for="check<?= $no; ?>">Pilih</label>
                                    </div>
                                </td>
                                <td>
                                    <?= $t->nama_lengkap; ?>
                                </td>
                                <td><?= $t->alamat; ?></td>
                                <td><?= $t->instansi; ?></td>
                                <td><?= $t->jabatan; ?></td>
                            </tr>
                        <?php $no++;
                        endforeach; ?>
                    </tbody>
                </table>
                <button class="btn btn-primary mt-2" type="submit"><i class="icon icon-left mdi mdi-save"></i> Simpan</button>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>