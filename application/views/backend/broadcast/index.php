<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">Daftar <?= $title; ?>
                <div class="tools dropdown">
                    <a href="<?= base_url($base . "/tambah_pesan"); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-plus text-white"></span> Tambah Data
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Judul</th>
                            <th>Tanggal Kirim</th>
                            <th width="20">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach ($data as $d) :
                        ?>
                            <tr class="odd gradeX">
                                <td><?= $no++; ?></td>
                                <td>
                                    <?= $d->judul; ?>
                                </td>
                                <td>
                                    <?= tgl_full($d->waktu_kirim); ?>
                                </td>
                                <td class="actions text-center">
                                    <div class="btn-group">
                                        <a title="Detail" class="icon mx-2" href="<?= base_url($base . "/detail/" . $d->broadcast_id); ?>">
                                            <i class="mdi mdi-view-list text-primary"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>