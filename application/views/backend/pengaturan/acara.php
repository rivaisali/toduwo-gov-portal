<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header"><?= $title; ?>
                <div class="tools dropdown">
                    <a href="<?= base_url($base . "/tambah_acara"); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-plus text-white"></span> Tambah Data
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Acara</th>
                            <th>Lokasi</th>
                            <th>Waktu</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($data as $d) : ?>
                            <tr class="odd gradeX">
                                <td><?= $no; ?></td>
                                <td><?= $d->nama_acara; ?></td>
                                <td><?= $d->nama_lokasi . "<br>" . $d->alamat_lokasi; ?></td>
                                <td><?= tgl_full($d->tanggal . " " . $d->jam); ?></td>
                                <td class="actions">
                                    <a class="icon mx-1" href="<?= base_url($base . "/ubah_acara/" . $d->acara_id); ?>">
                                        <i class="mdi mdi-edit"></i>
                                    </a>
                                    <?php if ($no > 1) { ?>
                                        <a class="icon mx-1" href="<?= base_url($base . "/hapus/" . $d->acara_id); ?>" id="hapusData">
                                            <i class="mdi mdi-delete text-danger"></i>
                                        </a>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php $no++;
                        endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>