<div class="row">
    <div class="col-12">
        <div class="card card-border-color card-border-color-primary">
            <!-- <div class="card-header card-header-divider"><?= $title; ?><span class="card-subtitle"></span></div> -->
            <div class="card-body">
                <?= form_open("", ["autocomplete" => "off", "enctype" => "multipart/form-data"]); ?>
                <?= form_hidden('id', $data->undangan_id); ?>
                <div class="row">
                    <div class="col-12 col-lg-12">
                        <div class="form-group pt-2">
                            <label for="judul">Judul Undangan</label>
                            <input class="form-control form-control-sm <?= form_error('judul') ? 'is-invalid' : ''; ?>" name="judul" id="judul" type="text" placeholder="Judul Undangan" value="<?= set_value('judul', $data->judul); ?>" autofocus>
                            <?= form_error('judul'); ?>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group pt-2">
                            <label for="nama_pp">Nama Pengantin Pria</label>
                            <input class="form-control form-control-sm <?= form_error('nama_pp') ? 'is-invalid' : ''; ?>" name="nama_pp" id="nama_pp" type="text" placeholder="Nama Pengantin Pria" value="<?= set_value('nama_pp', $data->nama_pengantin_pria); ?>">
                            <?= form_error('nama_pp'); ?>
                        </div>
                        <div class="form-group pt-2">
                            <label for="tentang_pp">Tentang Pengantin Pria</label>
                            <textarea name="tentang_pp" id="tentang_pp" class="form-control form-control-sm <?= form_error('tentang_pp') ? 'is-invalid' : ''; ?>" placeholder="Tentang Pengantin Pria"><?= set_value('tentang_pp', $data->tentang_pengantin_pria); ?></textarea>
                            <?= form_error('tentang_pp'); ?>
                        </div>
                        <div class="form-group pt-2">
                            <label for="tentang_pp">Foto Pengantin Pria</label>
                            <div class="row">
                                <div class="col-6 col-lg-4">
                                    <img src="<?= base_url('uploads/pengantin/' . $data->foto_pengantin_pria); ?>" alt="" class="img-thumbnail fotopp-preview">
                                </div>
                                <div class="col-6 col-lg-8">
                                    <!-- <div class="custom-file custom-file-sm"> -->
                                    <input type="file" name="foto_pp" class="form-control form-control-file form-control-sm <?= form_error('foto_pp') ? 'is-invalid' : ''; ?> mb-1" id="foto_pp" onchange="previewFotoPp()">
                                    <!-- <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div> -->
                                    <span class="text-muted">Kosongkan jika tidak ingin mengganti</span>
                                    <?= form_error('foto_pp'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6">
                        <div class="form-group pt-2">
                            <label for="nama_pw">Nama Pengantin Wanita</label>
                            <input class="form-control form-control-sm <?= form_error('nama_pw') ? 'is-invalid' : ''; ?>" name="nama_pw" id="nama_pw" type="text" placeholder="Nama Pengantin Wanita" value="<?= set_value('nama_pw', $data->nama_pengantin_wanita); ?>">
                            <?= form_error('nama_pw'); ?>
                        </div>
                        <div class="form-group pt-2">
                            <label for="tentang_pp">Tentang Pengantin Wanita</label>
                            <textarea name="tentang_pp" id="tentang_pp" class="form-control form-control-sm <?= form_error('tentang_pp') ? 'is-invalid' : ''; ?>" placeholder="Tentang Pengantin Wanita"><?= set_value('tentang_pp', $data->tentang_pengantin_wanita); ?></textarea>
                            <?= form_error('tentang_pp'); ?>
                        </div>
                        <div class="form-group pt-2">
                            <label for="tentang_pp">Foto Pengantin Wanita</label>
                            <div class="row">
                                <div class="col-6 col-lg-4">
                                    <img src="<?= base_url('uploads/pengantin/' . $data->foto_pengantin_wanita); ?>" alt="" class="img-thumbnail fotopw-preview">
                                </div>
                                <div class="col-6 col-lg-8">
                                    <!-- <div class="custom-file custom-file-sm"> -->
                                    <input type="file" name="foto_pw" class="form-control form-control-file form-control-sm <?= form_error('foto_pw') ? 'is-invalid' : ''; ?> mb-1" id="foto_pw" onchange="previewFotoPw()">
                                    <!-- <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div> -->
                                    <span class="text-muted">Kosongkan jika tidak ingin mengganti</span>
                                    <?= form_error('foto_pw'); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <button class="btn btn-space btn-primary" type="submit">Simpan</button>
                        <a href="<?= base_url("backend"); ?>" class="btn btn-space btn-secondary">Cancel</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>