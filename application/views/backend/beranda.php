<div class="alert alert-success alert-dismissible" role="alert">
    <div class="icon"><span class="mdi mdi-check"></span></div>
    <div class="message"><strong>Selamat Datang <?= user("nama"); ?></strong> di Aplikasi Toduwo Government Kabupaten Gorontalo</div>
</div>

<div class="row">
    <div class="col-lg-3">
        <div class="card text-center shadow bg-white rounded rounded-lg">
            <div class="card-body py-2 px-0">
                <span class="mdi mdi-accounts text-primary mb-1" style="font-size: 50px;"></span>
                <h1 class="m-0 p-0"><?= $tamu; ?></h1>
                <b>Tamu</b>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card text-center shadow bg-white rounded rounded-lg">
            <div class="card-body py-2 px-0">
                <span class="mdi mdi-collection-text text-success mb-1" style="font-size: 50px;"></span>
                <h1 class="m-0 p-0"><?= $undangan; ?></h1>
                <b>Undangan</b>
            </div>
        </div>
    </div>
    <?php if (user("level") == "admin") : ?>
        <div class="col-lg-3">
            <div class="card text-center shadow bg-white rounded rounded-lg">
                <div class="card-body py-2 px-0">
                    <span class="mdi mdi-accounts-list-alt text-danger mb-1" style="font-size: 50px;"></span>
                    <h1 class="m-0 p-0"><?= $pengguna; ?></h1>
                    <b>Pengguna</b>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>