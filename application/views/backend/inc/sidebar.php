<div class="be-left-sidebar">
    <div class="left-sidebar-wrapper">
        <div class="left-sidebar-spacer">
            <div class="left-sidebar-scroll">
                <div class="left-sidebar-content">
                    <ul class="sidebar-elements">
                        <li class="divider">Menu</li>
                        <li class=""><a href="<?= base_url("backend"); ?>"><i class="icon mdi mdi-home"></i><span>Dashboard</span></a></li>

                        <li class="">
                            <a href="<?= base_url("tamu"); ?>">
                                <i class="icon mdi mdi-accounts"></i>Tamu
                            </a>
                        </li>
                        <li class="">
                            <a href="<?= base_url("grup_tamu"); ?>">
                                <i class="icon mdi mdi-accounts-list-alt"></i>Grup Tamu
                            </a>
                        </li>
                        <li class="">
                            <a href="<?= base_url("undangan"); ?>">
                                <i class="icon mdi mdi-collection-text"></i>Undangan
                            </a>
                        </li>
                        <li class="">
                            <a href="<?= base_url("riwayat_undangan"); ?>">
                                <i class="icon mdi mdi-calendar"></i>Riwayat Undangan
                            </a>
                        </li>
                        <!-- <li class="">
                            <a href="<?= base_url("laporan"); ?>">
                                <i class="icon mdi mdi-view-subtitles"></i>Laporan
                            </a>
                        </li> -->
                        <?php if (user("level") == "admin") : ?>
                            <li class="">
                                <a href="<?= base_url("pengguna"); ?>">
                                    <i class="icon mdi mdi-accounts-list-alt"></i>Pengguna
                                </a>
                            </li>
                            <li class="">
                                <a href="<?= base_url("broadcast"); ?>">
                                    <i class="icon mdi mdi-mail-reply-all"></i>Broadcast
                                </a>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <!-- <div class="progress-widget">
            <div class="progress-data"><span class="progress-value">60%</span><span class="name">Current Project</span></div>
            <div class="progress">
                <div class="progress-bar progress-bar-primary" style="width: 60%;"></div>
            </div>
        </div> -->
    </div>
</div>