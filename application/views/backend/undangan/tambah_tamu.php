<div class="row">
    <div class="col-sm-12">
        <ul class="nav nav-tabs nav-justified mb-2" role="tablist">
            <li class="nav-item"><a class="nav-link active" href="#"><span class="icon mdi mdi-account"></span>Per Orang</a></li>
            <li class="nav-item"><a class="nav-link" href="<?= base_url($base . "/tambah_tamu_grup/" . $undangan->undangan_id); ?>"><span class="icon mdi mdi-accounts-list-alt"></span>Per Grup</a></li>
        </ul>
        <div class="card">
            <div class="card-header card-header-divider">
                Tambah Tamu
                <div class="tools dropdown">
                    <a href="<?= base_url($base . "/tambah_tamu_lain/" . $undangan->undangan_id); ?>" class="btn btn-space btn-warning">
                        <span class="icon icon-left mdi mdi-account-add text-white"></span> Tambah
                    </a>
                    <a href="<?= base_url($base . "/detail/" . $undangan->undangan_id); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-arrow-back text-white"></span> Kembali
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="bg-grey p-1">
                    Klik tombol <span class="btn btn-warning btn-xs disabled"><i class="icon icon-left mdi mdi-account-add text-white"></i> Tambah</span> Jika tamu yang anda cari tidak ada di dalam daftar.
                </div>
                <?= form_open(""); ?>
                <?= form_hidden('undangan_id', $undangan->undangan_id); ?>
                <?php
                if (form_error('tamu[]')) :
                    echo "<div class='alert alert-danger alert-dismissible mt-2' role='alert'>
                    <button class='close' type='button' data-dismiss='alert' aria-label='Close'><span class='mdi mdi-close' aria-hidden='true'></span></button>
                    <div class='icon'><span class='mdi mdi-close-circle-o'></span></div>
                    <div class='message'><strong>Error!</strong> " . form_error('tamu[]', "<span>", "</span>") . "</div>
                  </div>";
                endif;
                ?>
                <table class="table table-striped table-hover table-fw-widget bg-white" id="showall">
                    <thead>
                        <tr>
                            <th>
                                <div class="be-checkbox custom-control custom-checkbox">
                                    <input class="custom-control-input" type="checkbox" id="checkAll">
                                    <label class="custom-control-label ml-6" for="checkAll">Pilih Semua</label>
                                </div>
                            </th>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Instansi</th>
                            <th>Jabatan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($data as $t) : ?>
                            <tr>
                                <td>
                                    <div class="be-checkbox custom-control custom-checkbox">
                                        <input name="tamu[<?= $no; ?>]" value="<?= $t->tamu_id; ?>" class="custom-control-input" type="checkbox" id="check<?= $no; ?>" <?= set_checkbox("tamu[$no]", $t->tamu_id); ?> <?= ($t->whatsapp == "0") ? 'disabled' : ''; ?>>
                                        <label class="custom-control-label ml-6" for="check<?= $no; ?>">Pilih</label>
                                    </div>
                                </td>
                                <td>
                                    <?= $t->nama_lengkap; ?>
                                    <?php if ($t->whatsapp == "0") : ?>
                                        <span class="badge badge-danger d-inline-block">Bukan No. WA</span>
                                    <?php endif; ?>
                                </td>
                                <td><?= $t->alamat; ?></td>
                                <td><?= $t->instansi; ?></td>
                                <td><?= $t->jabatan; ?></td>
                            </tr>
                        <?php $no++;
                        endforeach; ?>
                    </tbody>
                </table>
                <button class="btn btn-primary mt-2" type="submit"><i class="icon icon-left mdi mdi-save"></i> Simpan</button>
                <?= form_close(); ?>
            </div>
        </div>
    </div>
</div>