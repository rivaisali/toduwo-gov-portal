<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">Daftar <?= $title; ?>
                <div class="tools dropdown">
                    <a href="<?= base_url($base . "/tambah"); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-plus text-white"></span> Tambah Data
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Judul</th>
                            <th>Waktu</th>
                            <th>Lokasi</th>
                            <th>Tamu</th>
                            <th>Buku Tamu</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($data as $d) : ?>
                            <tr class="odd gradeX">
                                <td><?= $no++; ?></td>
                                <td><?= $d->judul; ?></td>
                                <td>
                                    <?php if ($d->tanggal == $d->tanggal_selesai) {
                                        echo tgl_full($d->tanggal . " " . $d->jam) . " Sampai " . jam($d->tanggal_selesai . " " . $d->jam_selesai);
                                    } else {
                                        echo tgl_full($d->tanggal . " " . $d->jam) . " <br>s/d<br> " . tgl_full($d->tanggal_selesai . " " . $d->jam_selesai);
                                    }
                                    ?>
                                </td>
                                <td><?= $d->nama_lokasi; ?></td>
                                <td>
                                    <?= ambil_numrow_by_id("tamu_undangan", "undangan_id", $d->undangan_id); ?>
                                </td>
                                <td>
                                    <?php
                                    if ($d->tanggal > date("Y-m-d")) {
                                        echo "<span class='badge badge-danger'>Tutup</span>";
                                    } else {

                                    ?>
                                        <a title="Detail" class="icon mx-1" href="<?= base_url("bukutamu/" . $d->undangan_id); ?>">
                                            <i class="mdi mdi-view-list text-primary"></i> Buka
                                        </a>
                                    <?php
                                    }
                                    ?>
                                </td>
                                <td class="actions">
                                    <a title="Detail" class="icon mx-1" href="<?= base_url($base . "/detail/" . $d->undangan_id); ?>">
                                        <i class="mdi mdi-view-list text-primary"></i>
                                    </a>
                                    <a title="Tambah Tamu" class="icon mx-1" href="<?= base_url($base . "/tambah_tamu/" . $d->undangan_id); ?>">
                                        <i class="mdi mdi-account-add text-warning"></i>
                                    </a>
                                    <a title="Ubah" class="icon mx-1" href="<?= base_url($base . "/ubah/" . $d->undangan_id); ?>">
                                        <i class="mdi mdi-edit"></i>
                                    </a>
                                    <a title="Hapus" class="icon mx-1 delete" href="<?= base_url($base . "/hapus/" . $d->undangan_id); ?>" id="hapusData">
                                        <i class="mdi mdi-delete text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>