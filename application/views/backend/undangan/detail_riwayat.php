<div class="row">
    <div class="col-sm-12">
        <div class="user-info-list card">
            <div class="card-header card-header-divider">
                Data Undangan
                <div class="tools dropdown">
                    <a href="<?= base_url($base); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-arrow-back text-white"></span> Kembali
                    </a>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-6">
                    <table class="no-border no-strip skills">
                        <tbody class="no-border-x no-border-y">
                            <tr>
                                <td class="icon"><span class="mdi mdi-badge-check"></span></td>
                                <td class="item">Judul<span class="icon s7-portfolio"></span></td>
                                <td><?= $data->judul ?></td>
                            </tr>
                            <tr>
                                <td class="icon"><span class="mdi mdi-storage"></span></td>
                                <td class="item">Keterangan<span class="icon s7-gift"></span></td>
                                <td><?= $data->keterangan; ?></td>
                            </tr>
                            <tr>
                                <td class="icon"><span class="mdi mdi-calendar-check"></span></td>
                                <td class="item">Waktu<span class="icon s7-phone"></span></td>
                                <td>
                                    <?php if ($data->tanggal == $data->tanggal_selesai) {
                                        echo tgl_full($data->tanggal . " " . $data->jam) . " Sampai " . jam($data->tanggal_selesai . " " . $data->jam_selesai);
                                    } else {
                                        echo tgl_full($data->tanggal . " " . $data->jam) . " Sampai " . tgl_full($data->tanggal_selesai . " " . $data->jam_selesai);
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="icon"><span class="mdi mdi-home"></span></td>
                                <td class="item">Lokasi<span class="icon s7-map-marker"></span></td>
                                <td><?= $data->nama_lokasi; ?>
                                </td>
                            </tr>
                            <tr>
                                <td class="icon"><span class="mdi mdi-pin"></span></td>
                                <td class="item">Alamat<span class="icon s7-global"></span></td>
                                <td><?= $data->alamat_lokasi; ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">
                Daftar Tamu
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Kode Undangan</th>
                            <th>Nama</th>
                            <th>Alamat</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 1;
                        foreach ($tamu as $t) : ?>
                            <tr>
                                <td><?= $no++; ?></td>
                                <td><?= $t->tamu_undangan_id; ?></td>
                                <td><?= $t->nama_lengkap; ?></td>
                                <td><?= $t->alamat; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>