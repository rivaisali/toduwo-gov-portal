<div class="row">
    <div class="col-lg-12">
        <div class="card card-border-color card-border-color-primary">
            <!-- <div class="card-header card-header-divider"><?= $title; ?><span class="card-subtitle"></span></div> -->
            <div class="card-body">
                <?= form_open("", ["autocomplete" => "off", "enctype" => "multipart/form-data"]); ?>
                <div class="form-group pt-1">
                    <label for="judul">Judul</label>
                    <input class="form-control form-control-sm <?= form_error('judul') ? 'is-invalid' : ''; ?>" name="judul" id="judul" type="text" placeholder="Judul" value="<?= set_value('judul', ''); ?>">
                    <?= form_error('judul'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="keterangan">Keterangan</label>
                    <textarea name="keterangan" id="keterangan" rows="3" class="form-control form-control-sm <?= form_error('keterangan') ? 'is-invalid' : ''; ?>"><?= set_value('keterangan', ''); ?></textarea>
                    <?= form_error('keterangan'); ?>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group pt-1">
                            <label for="Waktu">Waktu Mulai</label>
                            <input class="form-control form-control-sm <?= form_error('waktu') ? 'is-invalid' : ''; ?> datetimepicker" name="waktu" id="waktu" type="text" placeholder="Pilih Tanggal" value="<?= set_value('waktu', ''); ?>">
                            <?= form_error('waktu'); ?>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group pt-1">
                            <label for="Waktu_selesai">Waktu Selesai</label>
                            <input class="form-control form-control-sm <?= form_error('waktu_selesai') ? 'is-invalid' : ''; ?> datetimepicker" name="waktu_selesai" id="waktu_selesai" type="text" placeholder="Pilih Tanggal" value="<?= set_value('waktu_selesai', ''); ?>">
                            <?= form_error('waktu_selesai'); ?>
                        </div>
                    </div>
                </div>
                <div class="form-group pt-1">
                    <label for="nama_lokasi">Nama Lokasi</label>
                    <input class="form-control form-control-sm <?= form_error('nama_lokasi') ? 'is-invalid' : ''; ?>" name="nama_lokasi" id="nama_lokasi" type="text" placeholder="Nama Lokasi" value="<?= set_value('nama_lokasi', ''); ?>">
                    <?= form_error('nama_lokasi'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="alamat_lokasi">Alamat Lokasi</label>
                    <input class="form-control form-control-sm <?= form_error('alamat_lokasi') ? 'is-invalid' : ''; ?>" name="alamat_lokasi" id="alamat_lokasi" type="text" placeholder="Alamat Lokasi" value="<?= set_value('alamat_lokasi', ''); ?>">
                    <?= form_error('alamat_lokasi'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="latlng_lokasi">Titik Lokasi (Google Maps)</label>
                    <input class="form-control form-control-sm <?= form_error('latlng_lokasi') ? 'is-invalid' : ''; ?>" name="latlng_lokasi" id="latlng_lokasi" type="text" placeholder="Titik Lokasi" value="<?= set_value('latlng_lokasi', ''); ?>">
                    <?= form_error('latlng_lokasi'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="latlng_lokasi">File Undangan (PDF)</label>
                    <input type="file" name="file_undangan" class="form-control form-control-file form-control-sm <?= form_error('file_undangan') ? 'is-invalid' : ''; ?> mb-1" id="file_undangan">
                    <?= form_error('file_undangan'); ?>
                </div>
                <div class="row pt-3">
                    <div class="col-sm-6">
                        <p class="text-left">
                            <button class="btn btn-space btn-primary" type="submit">Simpan</button>
                            <a href="<?= base_url($base); ?>" class="btn btn-space btn-secondary">Cancel</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>