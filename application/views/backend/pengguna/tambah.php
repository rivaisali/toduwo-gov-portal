<div class="row">
    <div class="col-lg-6">
        <div class="card card-border-color card-border-color-primary">
            <div class="card-body">
                <?= form_open("", ["autocomplete" => "off"]); ?>
                <div class="form-group pt-1">
                    <label for="nama">Nama</label>
                    <input class="form-control form-control-sm <?= form_error('nama') ? 'is-invalid' : ''; ?>" name="nama" id="nama" type="text" placeholder="Nama" value="<?= set_value('nama', ''); ?>">
                    <?= form_error('nama'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="email">Email</label>
                    <input class="form-control form-control-sm <?= form_error('email') ? 'is-invalid' : ''; ?>" name="email" id="email" type="text" placeholder="nama@example.com" value="<?= set_value('email', ''); ?>">
                    <?= form_error('email'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="password">Passwod</label>
                    <input class="form-control form-control-sm <?= form_error('password') ? 'is-invalid' : ''; ?>" name="password" id="password" type="text" placeholder="Passwod" value="<?= set_value('password', ''); ?>">
                    <?= form_error('password'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="no_telp">Telp / WA</label>
                    <input class="form-control form-control-sm <?= form_error('no_telp') ? 'is-invalid' : ''; ?>" name="no_telp" id="no_telp" type="text" placeholder="Masukkan No Telp" value="<?= set_value('no_telp', ''); ?>">
                    <?= form_error('no_telp'); ?>
                </div>
                <div class="row pt-3">
                    <div class="col-sm-6">
                        <p class="text-left">
                            <button class="btn btn-space btn-primary" type="submit">Simpan</button>
                            <a href="<?= base_url($base); ?>" class="btn btn-space btn-secondary">Cancel</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>