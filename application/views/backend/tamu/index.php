<div class="row">
    <div class="col-sm-12">
        <div class="card card-table">
            <div class="card-header">Daftar <?= $title; ?>
                <div class="tools dropdown">
                    <a href="<?= base_url($base . "/tambah"); ?>" class="btn btn-space btn-primary">
                        <span class="icon icon-left mdi mdi-plus text-white"></span> Tambah Data
                    </a>
                    <a href="<?= base_url($base . "/import"); ?>" class="btn btn-space btn-success">
                        <span class="icon icon-left mdi mdi-file-text text-white"></span>Import Excel
                    </a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped table-hover table-fw-widget" id="table4">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Alamat</th>
                            <th>Kontak</th>
                            <th>Instansi / Jabatan</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $d) : ?>
                            <tr class="odd gradeX">
                                <td><?= $d->nama_lengkap; ?></td>
                                <td><?= $d->alamat; ?></td>
                                <td>
                                    <?= ($d->email != "") ? $d->email . '<br>' : ''; ?>
                                    <?= $d->no_telp; ?>
                                    <?= ($d->whatsapp == "0") ? "<br><span class='badge badge-danger'>Bukan No WA</span>" : ""; ?>
                                </td>
                                <td>
                                    <?= ($d->instansi != "") ? $d->instansi . '<br>' : ''; ?>
                                    <?= $d->jabatan; ?>
                                </td>
                                <td class="actions">
                                    <a class="icon mx-1" href="<?= base_url($base . "/ubah/" . $d->tamu_id); ?>">
                                        <i class="mdi mdi-edit"></i>
                                    </a>
                                    <a class="icon mx-1 delete" href="<?= base_url($base . "/hapus/" . $d->tamu_id); ?>" id="hapusData">
                                        <i class="mdi mdi-delete text-danger"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>