<div class="row">
    <div class="col-lg-6">
        <div class="card card-border-color card-border-color-primary">
            <!-- <div class="card-header card-header-divider"><?= $title; ?><span class="card-subtitle"></span></div> -->
            <div class="card-body">
                <?= form_open("", ["autocomplete" => "off"]); ?>
                <div class="form-group pt-1">
                    <label for="nama">Nama</label>
                    <input class="form-control form-control-sm <?= form_error('nama') ? 'is-invalid' : ''; ?>" name="nama" id="nama" type="text" placeholder="Nama" value="<?= set_value('nama', ''); ?>">
                    <?= form_error('nama'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="nama">Alamat</label>
                    <textarea name="alamat" id="alamat" rows="3" class="form-control form-control-sm <?= form_error('alamat') ? 'is-invalid' : ''; ?>"><?= set_value('alamat', ''); ?></textarea>
                    <?= form_error('alamat'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="email">Email</label>
                    <input class="form-control form-control-sm <?= form_error('email') ? 'is-invalid' : ''; ?>" name="email" id="email" type="text" placeholder="nama@example.com" value="<?= set_value('email', ''); ?>">
                    <?= form_error('email'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="no_telp">Telp / WA</label>
                    <input class="form-control form-control-sm <?= form_error('no_telp') ? 'is-invalid' : ''; ?>" name="no_telp" id="no_telp" type="text" placeholder="Masukkan No Telp" value="<?= set_value('no_telp', ''); ?>">
                    <?= form_error('no_telp'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="instansi">Instansi / Organisasi</label>
                    <input class="form-control form-control-sm <?= form_error('instansi') ? 'is-invalid' : ''; ?>" name="instansi" id="instansi" type="text" placeholder="Instansi / Organisasi" value="<?= set_value('instansi', ''); ?>">
                    <?= form_error('instansi'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="jabatan">Jabatan</label>
                    <input class="form-control form-control-sm <?= form_error('jabatan') ? 'is-invalid' : ''; ?>" name="jabatan" id="jabatan" type="text" placeholder="Jabatan" value="<?= set_value('jabatan', ''); ?>">
                    <?= form_error('jabatan'); ?>
                </div>
                <div class="form-group pt-1">
                    <label for="grup">Grup</label>
                    <select class="select2 select2-sm" multiple="" name="grup[]">
                        <?php foreach ($grup as $g) : ?>
                            <option value="<?= $g->grup_tamu_id; ?>"><?= $g->nama_grup_tamu; ?></option>
                        <?php endforeach; ?>
                    </select>
                    <?= form_error('grup'); ?>
                </div>
                <div class="row pt-3">
                    <div class="col-sm-6">
                        <p class="text-left">
                            <button class="btn btn-space btn-primary" type="submit">Simpan</button>
                            <a href="<?= base_url($base); ?>" class="btn btn-space btn-secondary">Cancel</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>