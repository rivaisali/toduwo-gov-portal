  <!-- footer Start -->
    <section class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-4">
                    <div class="mb-4">
                        <a href="layout-one-1.html"><img src="images/logo-dark.png" alt="" class="logo-dark" height="26" /></a>
                        <p class="text-muted mt-4 mb-2">gov@toduwo.id</p>
                        <h6 class="text-muted font-weight-normal">+62 87816994929</h6>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-md-4">
                            <h6 class="footer-list-title text-dark mb-3">Layanan</h6>
                            <ul class="list-unstyled company-sub-menu">
                                <li><a href="">Undangan Digital</a></li>
                                <li><a href="">Buku Tamu Digital</a></li>
                                <li><a href="">Whatsapp Broadcast</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <h6 class="footer-list-title text-dark mb-3">About Us</h6>
                            <ul class="list-unstyled company-sub-menu">
                                <li><a href="">Contact Us</a></li>
                                <li><a href="">FAQs</a></li>
                                <li><a href="">Blog</a></li>
                                <li><a href="">Privacy Policy</a></li>
                            </ul>
                        </div>
                        <div class="col-md-4">
                            <h6 class="footer-list-title text-dark mb-3">Our Address</h6>
                            <p class="text-muted f-14">Jalan Taman Hiburan Kota Gorontalo</p>
                            <h6 class="text-muted pb-2">Email: Support@toduwo.id</h6>
                            <ul class="list-unstyled footer-social-list mt-4">
                                <li class="list-inline-item"><a href="#"><i class="mdi mdi-facebook"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="mdi mdi-instagram"></i></a></li>
                                <li class="list-inline-item"><a href="#"><i class="mdi mdi-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="text-center text-muted">
                        <p class="mb-0 f-15">2020 © Toduwo.ID.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- footer End -->

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="<?=base_url();?>assets/js/jquery.min.js"></script>
    <script src="<?=base_url();?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?=base_url();?>assets/js/scrollspy.min.js"></script>
    <script src="<?=base_url();?>assets/js/jquery.easing.min.js"></script>
    <!-- Counter js -->
    <script src="<?=base_url();?>assets/js/counter.int.js"></script>
        <!--Particles JS-->
    <script src="<?=base_url();?>assets/js/particles.js"></script>
    <script src="<?=base_url();?>assets/js/particles.app.js"></script>
    <!-- carousel -->
    <script src="<?=base_url();?>assets/js/owl.carousel.min.js"></script>
    <!-- Main Js -->
    <script src="<?=base_url();?>assets/js/app.js"></script>
  </body>

  </html>