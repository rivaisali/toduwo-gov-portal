<?php $this->load->view('frontend/inc/head_html'); ?>
<!-- Preloader -->
<div class="preloader">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="lds-hourglass"></div>
        </div>
    </div>
</div>
<!-- End Preloader -->

<div id="steps" class="easy-step-area pt-5 pb-5 bg-light">
    <div class="container">
        <div class="section-title">
            <span>Toduwo.id</span>
            <?php if ($tamu->dihadiri === null || $tamu->tamu_id == $tamu->dihadiri) : ?>
                <h2>Hai, <?= $orang->nama_lengkap; ?></h2>
                <p>Kami mengundang anda untuk dapat hadir pada kegiatan</p>
            <?php else : ?>
                <h2>Hai, <?= ambil_nama_by_id("tamu", "nama_lengkap", "tamu_id", $tamu->dihadiri); ?></h2>
                <p>Anda diminta oleh <span class="badge badge-primary text-white"><?= $orang->nama_lengkap; ?> </span> untuk menghadiri</p>
            <?php endif; ?>
            <h1><?= $undangan->judul; ?></h1>
        </div>

        <div class="row align-items-center pb-100">
            <div class="col-lg-6 d-none d-md-block">
                <div class="easy-step-img">
                    <img src="assets/images/konfirmasi.jpg" class="step-1" alt="Image">
                    <div class="easy-step-shape">
                        <img src="assets/images/shape/shape1.png" class="shape-1" alt="Shape">
                        <img src="assets/images/shape/shape21.png" class="shape-21" alt="Shape">
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="easy-step-card left-text">
                    <div class="row align-items-center">
                        <div class="col-lg-4 col-sm-4 col-4">
                            <div class="step-signle-card">
                                <i class="las la-calendar-check bg-1"></i>
                                <!-- <span>1</span> -->
                                <h3>Waktu</h3>
                            </div>
                        </div>
                        <div class="col-lg-8 col-sm-8 col-8">
                            <p>Acara akan dilaksanakan pada hari <?= hari($undangan->tanggal); ?>, tanggal <?= tgl_indonesia($undangan->tanggal); ?> pada jam <?= jam($undangan->tanggal . " " . $undangan->jam); ?></p>
                        </div>
                    </div>
                </div>

                <div class="easy-step-card right-text">
                    <div class="row align-items-center">
                        <div class="col-lg-4 col-sm-4 col-4">
                            <div class="step-signle-card">
                                <i class="las la-home bg-2"></i>
                                <!-- <span>2</span> -->
                                <h3>Lokasi</h3>
                            </div>
                        </div>
                        <div class="col-lg-8 col-sm-8 col-8">
                            <p>Lokasi acara bertempat di <?= $undangan->nama_lokasi; ?></p>
                        </div>
                    </div>
                </div>

                <div class="easy-step-card left-text">
                    <div class="row align-items-center">
                        <div class="col-lg-4 col-sm-4 col-4">
                            <div class="step-signle-card">
                                <i class="las la-map-marker bg-danger"></i>
                                <!-- <span>3</span> -->
                                <h3>Alamat</h3>
                            </div>
                        </div>
                        <div class="col-lg-8 col-sm-8 col-8">
                            <p><?= $undangan->alamat_lokasi; ?></p>
                        </div>
                    </div>
                </div>

                <div class="easy-step-card left-text">
                    <div class="row align-items-center">
                        <div class="col-lg-4 col-sm-4 col-4">
                            <div class="step-signle-card">
                                <i class="las la-clipboard-check bg-primary"></i>
                                <!-- <span>3</span> -->
                                <h3>Keterangan</h3>
                            </div>
                        </div>
                        <div class="col-lg-8 col-sm-8 col-8">
                            <p><?= $undangan->keterangan; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php if ($tamu->dihadiri === null) { ?>
            <h5 class="text-center">Mohon konfirmasi kedatangan anda.</h5>
            <div id="pesanKonfirmasi"></div>
            <div class="col-12 mx-auto text-center" id="tombolKonfirmasi">
                <a href="<?= base_url("datang/" . $tamu->tamu_undangan_id); ?>" class="btn btn-primary btn-sm" id="datang">Saya akan datang</a>
                <a href="javascript:void(0)" class="btn btn-danger btn-sm" id="btnWakili">Saya akan diwakili</a>
            </div>
        <?php } ?>
        <div class="col-md-6 col-sm-12 d-none mx-auto" id="boxDatang">
            <div class="my-2 p-4 rounded-lg shadow-lg">
                <div id="responseDiv">
                    <div id="message"></div>
                </div>
                <form class="" id="formDatang" method="POST">
                    <b>Verifikasi Data Anda</b>
                    <input type="hidden" name="tamu_undangan_id" value="<?= $tamu->tamu_undangan_id; ?>">
                    <div class="form-group">
                        <label for="telpTamu">No Telp/WA</label>
                        <input type="text" class="form-control" name="no_telp" id="telpTamu" placeholder="No Telp / WA" value="<?= $orang->no_telp; ?>" required="" readonly>
                    </div>
                    <div class="form-group">
                        <label for="namaTamu">Nama</label>
                        <input type="text" class="form-control" name="nama" id="namaTamu" placeholder="Nama Anda" value="<?= $orang->nama_lengkap; ?>" required="">
                    </div>
                    <div class="form-group">
                        <label for="alamatTamu">Alamat</label>
                        <textarea name="alamat" id="alamatTamu" rows="2" class="form-control"><?= $orang->alamat; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="emailTamu">Email</label>
                        <input type="email" class="form-control" name="email" id="emailTamu" placeholder="Email Anda" value="<?= $orang->email; ?>">
                    </div>
                    <div class="form-group">
                        <label for="instansiTamu">Instansi / Organisasi</label>
                        <input type="text" class="form-control" name="instansi" id="instansiTamu" placeholder="Instasnsi / Organisasi Anda" value="<?= $orang->instansi; ?>">
                    </div>
                    <div class="form-group">
                        <label for="jabatanTamu">Jabatan</label>
                        <input type="text" class="form-control" name="jabatan" id="jabatanTamu" placeholder="Jabatan Anda" value="<?= $orang->jabatan; ?>">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block" id="simpanDatang">Verifikasi</button>
                </form>
            </div>
        </div>
        <div class="col-md-6 col-sm-12 d-none mx-auto" id="boxWakili">
            <div class="my-2 p-4 rounded-lg shadow-lg">
                <div id="responseDivWakili">
                    <div id="messageWakili"></div>
                </div>
                <form class="" id="formWakili" method="POST">
                    <b>Isi data orang yang akan mewakili anda</b>
                    <input type="hidden" name="tamu_undangan_id" value="<?= $tamu->tamu_undangan_id; ?>">
                    <div class="form-group">
                        <label for="namaWakili">Nama</label>
                        <input type="text" class="form-control" name="nama" id="namaWakili" placeholder="Nama Yang Mewakili" required="">
                    </div>
                    <div class="form-group">
                        <label for="telpWakili">No Telp/WA</label>
                        <input type="text" class="form-control" name="no_telp" id="telpWakili" placeholder="No Telp / WA Yang Mewakili" required="">
                    </div>
                    <button type="submit" class="btn btn-primary btn-block" id="simpanWakili">Simpan</button>
                </form>
            </div>
        </div>

        <!-- tombol verifikasi tamu yang mewakili -->
        <?php if ($tamu->dihadiri !== null && $tamu->dihadiri != $tamu->tamu_id && $tamu->verifikasi_wakili === null) { ?>
            <h5 class="text-center">Mohon konfirmasi kedatangan anda.</h5>
            <div id="pesanKonfirmasi"></div>
            <div class="col-12 mx-auto text-center" id="tombolKonfirmasi">
                <a href="javascript:void(0)" class="btn btn-primary btn-sm" id="datangWakili">Saya akan datang</a>
            </div>
            <div class="col-md-6 col-sm-12 d-none mx-auto" id="boxDatangWakili">
                <div class="my-2 p-4 rounded-lg shadow-lg">
                    <div id="responseDivDatangWakili">
                        <div id="messageDatangWakili"></div>
                    </div>
                    <form class="" id="formDatangWakili" method="POST">
                        <b>Verifikasi Data Anda</b>
                        <input type="hidden" name="tamu_undangan_id" value="<?= $tamu->tamu_undangan_id; ?>">
                        <div class="form-group">
                            <label for="telpTamu">No Telp/WA</label>
                            <input type="text" class="form-control" name="no_telp" id="telpTamu" placeholder="No Telp / WA" value="<?= $wakili->no_telp; ?>" required="" readonly>
                        </div>
                        <div class="form-group">
                            <label for="namaTamu">Nama</label>
                            <input type="text" class="form-control" name="nama" id="namaTamu" placeholder="Nama Anda" value="<?= $wakili->nama_lengkap; ?>" required="">
                        </div>
                        <div class="form-group">
                            <label for="alamatTamu">Alamat</label>
                            <textarea name="alamat" id="alamatTamu" rows="2" class="form-control"><?= $wakili->alamat; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="emailTamu">Email</label>
                            <input type="email" class="form-control" name="email" id="emailTamu" placeholder="Email Anda" value="<?= $wakili->email; ?>">
                        </div>
                        <div class="form-group">
                            <label for="instansiTamu">Instansi / Organisasi</label>
                            <input type="text" class="form-control" name="instansi" id="instansiTamu" placeholder="Instasnsi / Organisasi Anda" value="<?= $wakili->instansi; ?>">
                        </div>
                        <div class="form-group">
                            <label for="jabatanTamu">Jabatan</label>
                            <input type="text" class="form-control" name="jabatan" id="jabatanTamu" placeholder="Jabatan Anda" value="<?= $wakili->jabatan; ?>">
                        </div>
                        <button type="submit" class="btn btn-primary btn-block" id="simpanDatangWakili">Verifikasi</button>
                    </form>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<?php $this->load->view('frontend/inc/foot_html'); ?>