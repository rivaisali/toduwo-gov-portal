 <!-- Hero Start -->
<section class="hero-6-bg position-relative vh-100 d-flex align-items-center" id="home">

        <div class="container">
            <div class="row align-items-center">

                 <div class="col-lg-6">
                      <div class="hero-6-title-content pr-lg-5">
                       <!--  <p class="text-uppercase text-primary font-weight-medium f-14 mb-4">Video Conference Kota Gorontalo</p> -->
                       <h1 class="hero-1-title mb-4 font-weight-normal line-height-1_4">Undangan Digital Pemerintahan Dengan<span class="text-primary font-weight-medium">Toduwo.id</span></h1>
                        <p class="text-muted mb-4 pb-2">Platform Undangan Digital Terlengkap untuk Solusi Aman mengirim undangan saat pandemi Covid-19.</p>
                        <a href="https://wa.me/6287816994929" class="btn btn-warning">Hubungi Admin<span class="ml-2 right-icon">&#8594;</span></a>
                     <a href="<?=base_url()?>login" class="btn btn-primary">Login Demo<span class="ml-2 right-icon">&#8594;</span></a>

                    </div>
                </div>
                  <div class="col-lg-6">
                    <div class="mt-12 mt-lg-0">
                        <img src="<?=base_url();?>assets/images/bg1.png" alt="" class="img-fluid mx-auto d-block">
                    </div>
                </div>

    </section>


    <!-- Service Start -->
    <section class="section" id="services">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <div class="title text-center mb-5">
                        <h3 class="font-weight-normal text-dark">Our <span class="text-warning">Service</span></h3>
                        <p class="text-muted">Layanan yang kami sediakan untuk Toduwo Goverment yaitu :</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="service-box service-warning">
                        <div class="mb-5">
                            <i class="pe-7s-headphones service-icon"></i>
                        </div>
                        <h5 class="service-title text-dark font-weight-normal pt-1 mb-4">Helpdesk 24 Jam</h5>
                        <p class="text-muted service-subtitle mb-4">Kami menyediakan layanan untuk tanya jawab 24 Jam.</p>
                        <a href="#" class="read-more-icon"><span class="right-icon">&#8594;</span></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="service-box service-warning">
                        <div class="mb-5">
                            <i class="pe-7s-tools service-icon"></i>
                        </div>
                        <h5 class="service-title text-dark font-weight-normal pt-1 mb-4">Solusi Persuratan Digital</h5>
                        <p class="text-muted service-subtitle mb-4">Toduwo Goverment menyediakan layanan kepada pemerintahan untuk Pengirman Surat Digital yang kekinian</p>
                        <a href="#" class="read-more-icon"><span class="right-icon">&#8594;</span></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="service-box service-warning">
                        <div class="mb-5">
                            <i class="pe-7s-display1 service-icon"></i>
                        </div>
                        <h5 class="service-title text-dark font-weight-normal pt-1 mb-4">Desain Modern</h5>
                        <p class="text-muted service-subtitle mb-4">Desain yang sangat modern untuk toduwo goverment.</p>
                        <a href="#" class="read-more-icon"><span class="right-icon">&#8594;</span></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="service-box service-warning">
                        <div class="mb-5">
                            <i class="pe-7s-paper-plane service-icon"></i>
                        </div>
                        <h5 class="service-title text-dark font-weight-normal pt-1 mb-4">Whatsapp Broadcast</h5>
                        <p class="text-muted service-subtitle mb-4">Toduwo Goverment menyediakan layanan untuk Pemerintahan melakukan broadcast undangan dengan whatsapp.</p>
                        <a href="#" class="read-more-icon"><span class="right-icon">&#8594;</span></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="service-box service-warning">
                        <div class="mb-5">
                            <i class="pe-7s-door-lock service-icon"></i>
                        </div>
                        <h5 class="service-title text-dark font-weight-normal pt-1 mb-4">Buku Tamu Digital</h5>
                        <p class="text-muted service-subtitle mb-4">Buku Tamu digital disediakan untuk Tamu yang diundang melalui Toduwo.gov untuk checkin dilokasi</p>
                        <a href="#" class="read-more-icon"><span class="right-icon">&#8594;</span></a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="service-box service-warning">
                        <div class="mb-5">
                            <i class="pe-7s-video service-icon"></i>
                        </div>
                        <h5 class="service-title text-dark font-weight-normal pt-1 mb-4">Video Conference <sup>Comingsoon</sup></h5>
                        <p class="text-muted service-subtitle mb-4">Toduwo Egoverment akan menyediakan video conference</p>
                        <a href="#" class="read-more-icon"><span class="right-icon">&#8594;</span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Service End -->

    <!-- About us Start -->
    <section class="section bg-light" id="about">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <div class="title text-center mb-5">
                        <h3 class="font-weight-normal text-dark">About <span class="text-warning">Us</span></h3>
                        <p class="text-muted">Toduwo Goverment merupakan layanan yang dibuat untuk memudahkan pemerintahan dalam kegiatan Undangan digital yang terintegrasi dengan buku tamu digital sehingga lebih memudahkan.</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <h2 class="font-weight-light line-height-1_6 text-dark mb-4">Toduwo Goverment hadir untuk Pemerintahan</h2>
                </div>
                <div class="col-md-7 offset-md-1">
                    <div class="row">
                        <div class="col-md-6">
                            <h6 class="text-dark font-weight-light f-20 mb-3">Our Mission</h6>
                            <p class="text-muted font-weight-light">Memberikan kemudahan kepada pemerintahan untuk Undangan Digital</p>
                        </div>
                        <div class="col-md-6">
                            <h6 class="text-dark font-weight-light f-20 mb-3">Our Vision</h6>
                            <p class="text-muted font-weight-light">Memberikan kemudahan kepada pemerintahan untuk Undangan Digital</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About us End -->

    <!-- Features Start -->
    <section class="section" id="features">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <div class="title text-center mb-5">
                        <h3 class="font-weight-normal text-dark">Our <span class="text-warning">Features</span></h3>
                        <p class="text-muted">Toduwo Goverment menyediakan fitur yang memudahkan dalam membuat dan mengirimkan undangan secara digital.</p>
                    </div>
                </div>
            </div>

            <div class="row align-items-center">
                <div class="col-md-5" style="background: url(<?=base_url();?>assets/images/features/features-bg.png) center center">
                    <div class="features-img">
                        <img src="<?=base_url();?>assets/images/features/img-1.png" alt="" class="img-fluid d-block mx-auto">
                    </div>
                </div>
                <div class="col-md-6 offset-md-1">
                    <div class="features-box mt-5 mt-sm-0 mb-4">
                        <div class="features-icon my-4">
                            <i class="mdi mdi-laptop-mac"></i>
                        </div>
                        <h5 class="text-dark font-weight-normal mb-3 pt-3">Management Laporan</h5>
                        <p class="text-muted mb-3 f-15">Laporan Undangan yang diorganisir dengan baik dan teratur</p>
                        <a href="#" class="f-16 text-warning">Read More <span class="right-icon ml-2">&#8594;</span></a>
                    </div>
                </div>
            </div>

            <div class="row align-items-center mt-5">
                <div class="col-md-6">
                    <div class="features-box mb-4">
                        <div class="features-icon my-4">
                            <i class="mdi mdi-account-group"></i>
                        </div>
                        <h5 class="text-dark font-weight-normal mb-3 pt-3">Manajemen Tamu</h5>
                        <p class="text-muted mb-3 f-15">Tamu undangan yang sudah termanajemen dengan baik sehingga mudah dalam melakukan pencarian</p>
                        <a href="#" class="f-16 text-warning">Read More <span class="right-icon ml-2">&#8594;</span></a>
                    </div>
                </div>
                <div class="col-md-5 offset-md-1 mt-5 mt-sm-0" style="background: url(<?=base_url();?>assets/images/features/features-bg.png) center center">
                    <div class="features-img">
                        <img src="<?=base_url();?>assets/images/features/img-2.png" alt="" class="img-fluid d-block mx-auto">
                    </div>
                </div>
            </div>

            <!-- <div class="row align-items-center mt-5">
                <div class="col-md-5" style="background: url(<?=base_url();?>assets/images/features/features-bg.png) center center">
                    <div class="features-img">
                        <img src="<?=base_url();?>assets/images/features/img-3.png" alt="" class="img-fluid d-block mx-auto">
                    </div>
                </div>
                <div class="col-md-6 offset-md-1">
                    <div class="features-box mt-5 mt-sm-0 mb-4">
                        <div class="features-icon my-4">
                            <i class="mdi mdi-chart-bell-curve"></i>
                        </div>
                        <h5 class="text-dark font-weight-normal mb-3 pt-3">Marketing Analysis</h5>
                        <p class="text-muted mb-3 f-15">At vero eos accusamus iusto odio soluta nobis est eligendi optio dignissimos ducimus qui blanditiis praesentium as voluptatum deleniti corrupti quos dolores molestias occaecati.</p>
                        <a href="#" class="f-16 text-warning">Read More <span class="right-icon ml-2">&#8594;</span></a>
                    </div>
                </div>
            </div> -->
        </div>
    </section>
    <!-- Features End -->


    <!-- Testimonial Start -->
    <section class="section bg-light" id="clients">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-6 col-md-8">
                    <div class="title text-center mb-5">
                        <h3 class="font-weight-normal text-dark">Our <span class="text-warning">Clients</span></h3>
                        <p class="text-muted">Dibawah ini merupakan Client yang sudah menggunakan layanan kami .</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <h3 class="font-weight-normal mt-3 line-height-1_4">Some Words From Our <span class="font-weight-medium text-warning">Happy Clients </span></h3>
                    <div class="testi-border my-4"></div>
                    <!-- <p class="text-muted">Itaque earum rerum tenetur a sapiente delectus ut aut reiciendis voluptatibus maiores alias consequatur.</p> -->
                </div>
                <div class="col-lg-8">
                    <div class="owl-carousel owl-theme">
                        <div class="item">
                            <div class="testi-content m-3 position-relative">
                                <div class="testi-box p-4">
                                    <ul class="list-unstyled f-15 text-warning mb-2">
                                        <li class="list-inline-item mr-1"><i class="mdi mdi-star"></i></li>
                                        <li class="list-inline-item mr-1"><i class="mdi mdi-star"></i></li>
                                        <li class="list-inline-item mr-1"><i class="mdi mdi-star"></i></li>
                                        <li class="list-inline-item mr-1"><i class="mdi mdi-star"></i></li>
                                        <li class="list-inline-item"><i class="mdi mdi-star"></i></li>
                                    </ul>
                                    <p class="text-muted position-relative mb-0 f-14"><span class="f-20 mr-1">"</span>Layanan undangan digital yang kekinian untuk Solusi dimasa Pandemi seperti sekarang. <span class="f-16">"</span></p>
                                </div>
                                <div class="testi-user mt-4">
                                    <div class="media align-items-center">
                                        <img src="https://upload.wikimedia.org/wikipedia/commons/a/a6/Lambang_Kabupaten_Gorontalo.jpg" alt="" class="mr-3 img-fluid d-block rounded-circle">
                                        <div class="media-body">
                                            <h6 class="mb-0 text-dark f-15">Pemerintah Kab. Gorontalo</h6>
                                            <p class="text-muted mb-0 f-14">Kabupaten</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- owl item and -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonial End -->


    <!-- Subscribe Start -->
    <section class="section bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-7">
                    <div class="row">
                        <div class="col-4">
                            <div class="subscribe-icon">
                                <img src="<?=base_url();?>assets/images/icon/1.png" alt="" class="img-fluid mx-auto d-block">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="subscribe-icon">
                                <img src="<?=base_url();?>assets/images/icon/2.png" alt="" class="img-fluid mx-auto d-block">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="">
                                <img src="<?=base_url();?>assets/images/icon/3.png" alt="" class="img-fluid mx-auto d-block">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="text-center mt-5 mb-4">
                        <h6 class="text-muted font-weight-normal">Subscribe To Our Newsletter For New Content, <span class="d-block mt-2">Update And Giveaways!</span></h6>
                    </div>
                    <div class="text-center subscribe-form mt-4">
                        <form action="#">
                            <input type="text" placeholder="Your Email Address...">
                            <button type="submit" class="btn btn-warning">Subscribe</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Subscribe End -->
