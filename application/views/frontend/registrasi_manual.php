<?php $this->load->view('frontend/inc/head_html'); ?>
<!-- Preloader -->
<div class="preloader">
    <div class="d-table">
        <div class="d-table-cell">
            <div class="lds-hourglass"></div>
        </div>
    </div>
</div>
<!-- End Preloader -->

<div id="steps" class="easy-step-area pt-5 pb-5 bg-light">
    <div class="container">
        <div class="section-title">
            <span>Toduwo.id</span>
            <h1><?= $undangan->judul; ?></h1>
        </div>

        <div class="row align-items-start pb-100">
            <div class="col-lg-6">
                <div class="my-2 p-4 rounded-lg shadow-lg">
                    <div id="responseDiv">
                        <div id="message"></div>
                    </div>
                    <form class="" method="POST">
                        <b>Mendaftar </b>
                        <input type="hidden" name="undangan_id" value="<?= $undangan->undangan_id; ?>">
                        <input type="hidden" name="user_id" value="<?= $undangan->user_id; ?>">
                        <div class="form-group">
                            <label for="telpTamu">No Telp/WA</label>
                            <input type="text" class="form-control <?= form_error('no_telp') ? 'is-invalid' : ''; ?>" name="no_telp" id="telpTamu" placeholder="No Telp / WA">
                            <?= form_error('no_telp'); ?>
                        </div>
                        <div class="form-group">
                            <label for="namaTamu">Nama</label>
                            <input type="text" class="form-control <?= form_error('nama') ? 'is-invalid' : ''; ?>" name="nama" id="namaTamu" placeholder="Nama Anda">
                            <?= form_error('nama'); ?>
                        </div>
                        <div class="form-group">
                            <label for="alamatTamu">Alamat</label>
                            <textarea name="alamat" id="alamatTamu" rows="2" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="emailTamu">Email</label>
                            <input type="email" class="form-control" name="email" id="emailTamu" placeholder="Email Anda"">
                    </div>
                    <div class=" form-group">
                            <label for="instansiTamu">Instansi / Organisasi</label>
                            <input type="text" class="form-control" name="instansi" id="instansiTamu" placeholder="Instasnsi / Organisasi Anda">
                        </div>
                        <div class="form-group">
                            <label for="jabatanTamu">Jabatan</label>
                            <input type="text" class="form-control" name="jabatan" id="jabatanTamu" placeholder="Jabatan Anda">
                        </div>
                        <button type="submit" class="btn btn-primary btn-block" id="simpanDatangWakili">Daftar</button>
                    </form>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="easy-step-card left-text">
                    <div class="row align-items-center">
                        <div class="col-lg-4 col-sm-4 col-4">
                            <div class="step-signle-card">
                                <i class="las la-calendar-check bg-1"></i>
                                <!-- <span>1</span> -->
                                <h3>Waktu</h3>
                            </div>
                        </div>
                        <div class="col-lg-8 col-sm-8 col-8">
                            <p>Acara akan dilaksanakan pada hari <?= hari($undangan->tanggal); ?>, tanggal <?= tgl_indonesia($undangan->tanggal); ?> pada jam <?= jam($undangan->tanggal . " " . $undangan->jam); ?></p>
                        </div>
                    </div>
                </div>

                <div class="easy-step-card right-text">
                    <div class="row align-items-center">
                        <div class="col-lg-4 col-sm-4 col-4">
                            <div class="step-signle-card">
                                <i class="las la-home bg-2"></i>
                                <!-- <span>2</span> -->
                                <h3>Lokasi</h3>
                            </div>
                        </div>
                        <div class="col-lg-8 col-sm-8 col-8">
                            <p>Lokasi acara bertempat di <?= $undangan->nama_lokasi; ?></p>
                        </div>
                    </div>
                </div>

                <div class="easy-step-card left-text">
                    <div class="row align-items-center">
                        <div class="col-lg-4 col-sm-4 col-4">
                            <div class="step-signle-card">
                                <i class="las la-map-marker bg-danger"></i>
                                <!-- <span>3</span> -->
                                <h3>Alamat</h3>
                            </div>
                        </div>
                        <div class="col-lg-8 col-sm-8 col-8">
                            <p><?= $undangan->alamat_lokasi; ?></p>
                        </div>
                    </div>
                </div>

                <div class="easy-step-card left-text">
                    <div class="row align-items-center">
                        <div class="col-lg-4 col-sm-4 col-4">
                            <div class="step-signle-card">
                                <i class="las la-clipboard-check bg-primary"></i>
                                <!-- <span>3</span> -->
                                <h3>Keterangan</h3>
                            </div>
                        </div>
                        <div class="col-lg-8 col-sm-8 col-8">
                            <p><?= $undangan->keterangan; ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<?php $this->load->view('frontend/inc/foot_html'); ?>