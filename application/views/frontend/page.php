<!doctype html>
<html lang="en">

<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="<?= base_url('assets/fonts/font.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/sipardi.css'); ?>">
	<!-- <link rel="stylesheet" href="<?= base_url('assets/css/bootstrap.min.css'); ?>"> -->
	<link rel="stylesheet" href="<?= base_url('assets/css/modern-bussines.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/custom/style.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/fontawesome/css/all.css'); ?>">

	<!-- owl carousel -->
	<link rel="stylesheet" href="<?= base_url('assets/css/owl.carousel.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/owl.theme.default.min.css'); ?>">

	<!-- datepicker -->
	<link rel="stylesheet" href="<?= base_url('assets/css/bootstrap-datepicker3.standalone.min.css'); ?>">

	<!-- datatable -->
	<link rel="stylesheet" href="<?= base_url('assets/datatables/datatables.min.css'); ?>">

	<!-- select2 -->
	<link rel="stylesheet" href="<?= base_url('assets/css/select2.min.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/css/select2-bootstrap4.css'); ?>">

	<!-- slick -->
	<link rel="stylesheet" href="<?= base_url('assets/slick/slick.css'); ?>">
	<link rel="stylesheet" href="<?= base_url('assets/slick/slick-theme.css'); ?>">

	<!-- animated -->
	<link rel="stylesheet" href="<?= base_url('assets/css/animate.min.css'); ?>">

	<title>SIPARDI</title>
	<script>
		const url = '<?php echo base_url(); ?>';
	</script>
</head>

<body class="d-flex flex-column bg-light">
	<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-white" id="navbar-top">
		<!-- <div class="container"> -->
		<a class="navbar-brand w-25 text-left" href="<?= base_url(); ?>">
			<img src="<?= base_url('assets/img/logo.png'); ?>" alt="Sipardi" class="logo-brand">
		</a>

		<button class="navbar-toggler navbar-toggler-right border-0" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
			<span class="text-primary"><i class="fas fa-bars fa-lg"></i></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarResponsive">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item">
					<a class="nav-link" title="Keranjang Belanja" href="">
						<span>Tentang Sipardi</span>
					</a>
				</li>
			</ul>
		</div>

	</nav>

	<main class="" id="page-content">
		<div class="container mt-5 pt-5">
			<div class="bg-white shadow p-5">
				<?= $data->$page; ?>
			</div>

		</div>
	</main>

	<?php $this->load->view("frontend/inc/footer"); ?>
	<!-- <script src="<?= base_url('assets/js/jquery-3.5.1.min.js'); ?>"></script> -->
	<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
	<script src="<?= base_url('assets/js/popper.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<!-- owl carousel -->
	<script src="<?= base_url('assets/js/owl.carousel.min.js'); ?>"></script>
	<!-- datepicker -->
	<script src="<?= base_url('assets/js/bootstrap-datepicker.min.js'); ?>"></script>
	<!-- datatable -->
	<script src="<?= base_url('assets/datatables/datatables.min.js'); ?>"></script>
	<!-- select2 -->
	<script src="<?= base_url('assets/js/select2.min.js'); ?>"></script>
	<!-- Slick -->
	<script src="<?= base_url('assets/slick/slick.js'); ?>"></script>
	<!-- <script src="https://cdn.jsdelivr.net/jquery.slick/1.5.5/slick.min.js"></script> -->

	<script src="<?= base_url('assets/js/custom/my.js'); ?>"></script>
</body>

</html>