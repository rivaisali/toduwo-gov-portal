<?php $this->load->view("frontend/inc/head_html"); ?>
<div id="features" class="our-services-area service-shape pt-70 pb-70">
    <div class="container">
        <div class="section-title">
            <span>Toduwo.id</span>
            <?php if ($this->session->flashdata("notifikasi")) { ?>
                <div class="alert alert-<?= $this->session->flashdata("notifikasi")["status"] ?>" role="alert">
                    <h4 class="alert-heading">
                        <?php if ($this->session->flashdata("notifikasi")["status"] == "success") { ?>
                            <span class="las la-check-circle"></span> Selamat!
                        <?php } else { ?>
                            <span class="las la-times-circle"></span> Maaf!
                        <?php } ?>
                    </h4>
                    <p><?= $this->session->flashdata("notifikasi")["msg"]; ?></p>
                </div>
            <?php } ?>
            <!-- <h2></h2>
            <p>Undangan ini terdaftar di Toduwo.id dengan rincian seperti dibawah ini</p> -->
        </div>

        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="our-service-card">
                    <i class="las la-user-tie bg-1 m-0 mb-2"></i>
                    <h3 class="m-0 p-0">Panitia</h3>
                    <p><?= ambil_nama_by_id("user", "nama", "user_id", $undangan->user_id); ?></p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="our-service-card">
                    <i class="las la-certificate bg-2 m-0 mb-2"></i>
                    <h3 class="m-0 p-0">Judul</h3>
                    <p><?= $undangan->judul; ?></p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="our-service-card">
                    <i class="las la-calendar bg-3 m-0 mb-2"></i>
                    <h3 class="m-0 p-0">Waktu</h3>
                    <p><?= tgl_full($undangan->tanggal . " " . $undangan->jam); ?></p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="our-service-card">
                    <i class="las la-map-marker bg-4 m-0 mb-2"></i>
                    <h3 class="m-0 p-0">Lokasi</h3>
                    <p><?= $undangan->nama_lokasi; ?></p>
                </div>
            </div>
        </div>
        <div class="our-service-shape">
            <img src="<?= base_url("assets/images/shape/shape26.png"); ?>" alt="Shape">
        </div>
    </div>
</div>
<?php $this->load->view("frontend/inc/foot_html"); ?>