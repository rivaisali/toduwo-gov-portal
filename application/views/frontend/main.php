<?php $this->load->view('frontend/inc/head_html');?>
<!-- Preloader -->
        <div class="preloader">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="lds-hourglass"></div>
                </div>
            </div>
        </div>
        <!-- End Preloader -->
<?php $this->load->view('frontend/inc/header');?>
<?php $this->load->view('frontend/' . $page);?>

<?php $this->load->view('frontend/inc/foot_html');?>