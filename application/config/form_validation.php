<?php

defined('BASEPATH') or exit('No direct script access allowed');

$config = array(
    'grup_tamu' => [
        [
            'field' => 'nama',
            'label' => 'Nama Grup Tamu',
            'rules' => 'trim|required',
        ],
        [
            'field' => 'deskripsi',
            'label' => 'Deskripsi',
            'rules' => 'trim',
        ],
    ],
    'grup_tamu_detail' => [
        [
            'field' => 'tamu[]',
            'label' => 'Tamu',
            'rules' => 'trim|callback_tamu_check',
        ],
    ],
    'tamu' => [
        [
            'field' => 'nama',
            'label' => 'Nama Tamu',
            'rules' => 'trim|required',
        ],
        [
            'field' => 'alamat',
            'label' => 'Alamat',
            'rules' => 'trim',
        ],
        [
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|valid_email',
        ],
        [
            'field' => 'no_telp',
            'label' => 'Telp / WA',
            'rules' => 'trim|required|numeric|min_length[10]|max_length[14]|callback_check_nomor',
        ],
        [
            'field' => 'instansi',
            'label' => 'Instansi / Organisasi',
            'rules' => 'trim',
        ],
        [
            'field' => 'jabatan',
            'label' => 'Jabatan',
            'rules' => 'trim',
        ],

    ],
    'pengguna' => [
        [
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'trim|required',
        ],
        [
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email|is_unique[user.email]',
        ],
        [
            'field' => 'no_telp',
            'label' => 'Telp / WA',
            'rules' => 'trim|required|numeric|min_length[11]|max_length[14]',
        ],
        [
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|required|min_length[8]',
        ],
    ],
    'pengguna_ubah' => [
        [
            'field' => 'nama',
            'label' => 'Nama',
            'rules' => 'trim|required',
        ],
        [
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|required|valid_email|callback_check_email',
        ],
        [
            'field' => 'no_telp',
            'label' => 'Telp / WA',
            'rules' => 'trim|required|numeric|min_length[11]|max_length[14]',
        ],
        [
            'field' => 'password',
            'label' => 'Password',
            'rules' => 'trim|min_length[8]',
        ],
    ],
    'penerima_tamu' => [
        [
            'field' => 'nama',
            'label' => 'Nama Penerima Tamu',
            'rules' => 'trim|required',
        ],
    ],
    'undangan' => [
        [
            'field' => 'judul',
            'label' => 'Judul',
            'rules' => 'trim|required',
        ],
        [
            'field' => 'keterangan',
            'label' => 'Keterangan',
            'rules' => 'trim',
        ],
        [
            'field' => 'waktu',
            'label' => 'Waktu',
            'rules' => 'trim|required|callback_tgl_check',
        ],
        [
            'field' => 'waktu_selesai',
            'label' => 'Waktu Selesai',
            'rules' => 'trim|required',
        ],
        [
            'field' => 'nama_lokasi',
            'label' => 'Nama Lokasi',
            'rules' => 'trim|required',
        ],
        [
            'field' => 'alamat_lokasi',
            'label' => 'Alamat Lokasi',
            'rules' => 'trim|required',
        ],
        [
            'field' => 'latlng_lokasi',
            'label' => 'Titik Lokasi',
            'rules' => 'trim',
        ],
        [
            'field' => 'file_undangan',
            'label' => 'File Undangan',
            'rules' => 'callback_check_file[file_undangan]',
        ],
    ],
    'tamu_undangan_banyak' => [
        [
            'field' => 'tamu[]',
            'label' => 'Tamu',
            'rules' => 'trim|callback_tamu_check',
        ],
    ],
    'tamu_undangan_grup' => [
        [
            'field' => 'tamu[]',
            'label' => 'Tamu',
            'rules' => 'trim|callback_tamu_check',
        ],
    ],
    'password_ubah' => [
        [
            'field' => 'password_lama',
            'label' => 'Password Lama',
            'rules' => 'trim|required|callback_passlama_check',
        ],
        [
            'field' => 'password_baru',
            'label' => 'Password Baru',
            'rules' => 'trim|required|min_length[8]',
        ],
        [
            'field' => 'ulangi_password_baru',
            'label' => 'Ulangi Password Baru',
            'rules' => 'trim|required|matches[password_baru]',
        ],
    ],
    'registrasi_manual' => [
        [
            'field' => 'nama',
            'label' => 'Nama Tamu',
            'rules' => 'trim|required',
        ],
        [
            'field' => 'alamat',
            'label' => 'Alamat',
            'rules' => 'trim',
        ],
        [
            'field' => 'email',
            'label' => 'Email',
            'rules' => 'trim|valid_email',
        ],
        [
            'field' => 'no_telp',
            'label' => 'Telp / WA',
            'rules' => 'trim|required|numeric|min_length[10]|max_length[14]',
        ],
        [
            'field' => 'instansi',
            'label' => 'Instansi / Organisasi',
            'rules' => 'trim',
        ],
        [
            'field' => 'jabatan',
            'label' => 'Jabatan',
            'rules' => 'trim',
        ],

    ],
    'pesan_broadcast' => [
        [
            'field' => 'judul',
            'label' => 'Judul',
            'rules' => 'trim|required',
        ],
        [
            'field' => 'isi_pesan',
            'label' => 'Isi Pesan',
            'rules' => 'trim|required',
        ],
        [
            'field' => 'gambar',
            'label' => 'Gambar',
            'rules' => 'callback_check_file[gambar]',
        ],
        [
            'field' => 'caption',
            'label' => 'Caption',
            'rules' => 'trim',
        ],
    ],
    'penerima_broadcast' => [
        [
            'field' => 'tamu[]',
            'label' => 'Tamu',
            'rules' => 'trim|callback_tamu_check',
        ],
    ],
);

$config['error_prefix'] = '<span class="clearfix invalid-feedback">';
$config['error_sufix'] = '</span>';

/* End of file form_validation.php */

/* Location: ./application/config/form_validation.php */
