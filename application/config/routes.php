<?php
defined('BASEPATH') or exit('No direct script access allowed');
$route['default_controller'] = 'frontend';
$route['404_override'] = 'e404';
$route['translate_uri_dashes'] = true;

$route['login'] = 'login/index';
$route['logout'] = 'login/logout';
$route['backend'] = 'Backend/index';
// rute tamu
$route['tamu'] = 'Tamu/index';
$route['tamu/tambah'] = 'Tamu/tambah';
$route['tamu/ubah/(:num)'] = 'Tamu/ubah/$1';
$route['tamu/hapus/(:num)'] = 'Tamu/hapus/$1';

// rute group tamu
$route['grup_tamu'] = 'Grup_tamu/index';
$route['grup_tamu/(:num)'] = 'Grup_tamu/index/$1';
$route['grup_tamu/tambah'] = 'Grup_tamu/tambah';
$route['grup_tamu/tamu/(:num)'] = 'Grup_tamu/tambah_tamu/$1';
$route['grup_tamu/ubah/(:num)'] = 'Grup_tamu/ubah/$1';
$route['grup_tamu/hapus/(:num)'] = 'Grup_tamu/hapus/$1';
$route['grup_tamu/hapus_tamu/(:num)/(:num)'] = 'Grup_tamu/hapus_tamu/$1/$2';

// rute pengguna
$route['pengguna'] = 'Pengguna/index';
$route['pengguna/tambah'] = 'Pengguna/tambah';
$route['pengguna/toggle/(:num)'] = 'Pengguna/toggle/$1';
$route['pengguna/ubah/(:num)'] = 'Pengguna/ubah/$1';
$route['pengguna/reset/(:num)'] = 'Pengguna/reset/$1';
$route['pengguna/hapus/(:num)'] = 'Pengguna/hapus/$1';

// rute undangan
$route['undangan'] = 'Undangan/index';
$route['undangan/detail/(:any)'] = 'Undangan/detail/$1';
$route['undangan/tambah'] = 'Undangan/tambah';
$route['undangan/ubah/(:any)'] = 'Undangan/ubah/$1';
$route['undangan/tambah_tamu/(:any)'] = 'Undangan/tambah_tamu/$1';
$route['undangan/tambah_tamu_grup/(:any)'] = 'Undangan/tambah_tamu_grup/$1';
$route['undangan/tambah_tamu_lain/(:any)'] = 'Undangan/tambah_tamu_lain/$1';
$route['undangan/hapus_tamu/(:any)'] = 'Undangan/hapus_tamu/$1';
$route['undangan/get_autocomplete'] = 'Undangan/get_autocomplete';
$route['undangan/kirimUndangan_banyak/(:any)'] = 'Undangan/kirimUndangan_banyak/$1';
$route['undangan/kirimUndangan/(:any)'] = 'Undangan/kirimUndangan/$1';

// rute riwayat undangan
$route['riwayat_undangan'] = 'Undangan/riwayat_undangan';
$route['detail_riwayat/(:any)'] = 'Undangan/detail_riwayat/$1';
$route['detail_absen/(:any)'] = 'Undangan/absen/$1';
$route['cetak_absen/(:any)/(:any)/(:any)'] = 'Undangan/cetak_absen/$1/$2/$3';
$route['export_xls/(:any)/(:any)/(:any)'] = 'Undangan/export_xls/$1/$2/$3';

// rute broadcast
$route['broadcast'] = 'Broadcast/index';
$route['broadcast/tambah_pesan'] = 'Broadcast/tambah_pesan';
$route['broadcast/detail/(:any)'] = 'Broadcast/detail/$1';

// rute untuk BukuTamu
$route['bukutamu/(:any)'] = 'Buku_tamu/index/$1';
$route['bukutamu/tamu/(:any)'] = 'Buku_tamu/tamu/$1';
$route['bukutamu/checkin/(:any)'] = 'Buku_tamu/checkin/$1';

// rute untuk konfirmasi hadir
$route['registrasi/get_autocomplete'] = 'Registrasi/get_autocomplete';
$route['registrasi_manual/(:any)'] = 'Registrasi/registrasi_manual/$1';
$route['registrasi_selesai/(:any)'] = 'Registrasi/registrasi_selesai/$1';
$route['datang'] = 'Registrasi/datang';
$route['simpan_wakili'] = 'Registrasi/simpan_wakili';
$route['datang_wakili'] = 'Registrasi/datang_wakili';
$route['testing'] = 'Testing/index';
$route['undangan/(:any)'] = 'Cek_undangan/index/$1';
$route['(:any)'] = 'Registrasi/index/$1';
