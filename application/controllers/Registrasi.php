<?php
class Registrasi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
    }

    // konfirmasi kehadiran
    public function index($id = null)
    {
        $tamu           =   $this->crud_model->select_one("tamu_undangan", "tamu_undangan_id", $id);
        if (empty($tamu)) {
            // $data["tamu"]   =   [];
            // redirect();
            $data['title']    =    "Halaman Tidak Ditemukan";
            $this->load->view('e404', $data);
        } else {
            $data["tamu"]       =   $tamu;
            $data["orang"]      =   $this->crud_model->select_one("tamu", "tamu_id", $tamu->tamu_id);
            $data["undangan"]   =   $this->crud_model->select_one("undangan", "undangan_id", $tamu->undangan_id);
            if ($tamu->dihadiri != $tamu->tamu_id) {
                $data["wakili"]      =   $this->crud_model->select_one("tamu", "tamu_id", $tamu->dihadiri);
            }
            $this->crud_model->update("tamu_undangan", ["lihat_undangan" => "1"], "tamu_undangan_id", $id);
            $this->load->view("frontend/registrasi", $data);
        }
    }

    function get_autocomplete()
    {
        if ($this->input->post('search', true)) {
            $result = $this->crud_model->select_like("tamu", "nama_lengkap", $this->input->post('search', true), "no_telp");
            if (count($result) > 0) {
                foreach ($result as $row)
                    $arr_result[] = array(
                        'value'         => $row->nama_lengkap,
                        'nama'         => $row->nama_lengkap,
                        'no_telp'   => $row->no_telp,
                    );
                echo json_encode($arr_result);
            }
        }
    }

    public function datang()
    {
        $tamu_undangan_id   =   $this->input->post('tamu_undangan_id', true);
        $cek_tamu   =   $this->crud_model->select_one("tamu_undangan", "tamu_undangan_id", $tamu_undangan_id);
        if (empty($cek_tamu)) {
            $ret = [
                "status" => "0",
                "message" => "Tamu tidak ditemukan",
                "error"  => true
            ];
        } else {
            $nama   =   $this->input->post('nama', true);
            $no_telp   =   $this->input->post('no_telp', true);
            $alamat   =   $this->input->post('alamat', true);
            $email   =   $this->input->post('email', true);
            $instansi   =   $this->input->post('instansi', true);
            $jabatan   =   $this->input->post('jabatan', true);
            $this->crud_model->update("tamu_undangan", ["dihadiri" => $cek_tamu->tamu_id], "tamu_undangan_id", $tamu_undangan_id);
            $this->crud_model->update("tamu", [
                "nama_lengkap" => $nama,
                "alamat" => $alamat,
                "email" => $email,
                "instansi" => $instansi,
                "jabatan" => $jabatan
            ], "tamu_id", $cek_tamu->tamu_id);
            // $undangan = $this->crud_model->select_one("undangan", "undangan_id", $cek_tamu->undangan_id);
            $this->load->library("whatsapp");
            $this->whatsapp->sendImage($no_telp, "Tunjukkan Code Ini Pada Penerima Tamu", base_url("uploads/qrcode/" . $tamu_undangan_id . ".png"));
            $ret = [
                "status" => "1",
                "message" => "Terima Kasih"
            ];
        }
        echo json_encode($ret);
    }

    public function simpan_wakili()
    {
        $tamu_undangan_id   =   $this->input->post('tamu_undangan_id', true);
        $nama   =   $this->input->post('nama', true);
        $no_telp   =   $this->input->post('no_telp', true);

        $cek_tamu   =   $this->crud_model->select_one("tamu", "nama_lengkap", $nama);
        if (empty($cek_tamu)) { // jika tamu tidak terdaftar
            $this->load->library('whatsapp');
            $nomor = json_decode($this->whatsapp->checkNumber($no_telp), true);
            if ($nomor["status"] === TRUE) {
                $data_tamu   =   [
                    "tamu_id"   =>  $this->crud_model->cek_id("tamu", "tamu_id"),
                    "nama_lengkap"  =>  $nama,
                    "no_telp"   =>  $no_telp
                ];
                $simpan_tamu = $this->crud_model->insert("tamu", $data_tamu);
                if ($simpan_tamu) {
                    $tamu_undangan = [
                        "dihadiri"  =>  $data_tamu["tamu_id"]
                    ];
                    $this->crud_model->update("tamu_undangan", $tamu_undangan, "tamu_undangan_id", $tamu_undangan_id);
                    $data_tamu_undangan = $this->crud_model->select_one("tamu_undangan", "tamu_undangan_id", $tamu_undangan_id);
                    $undangan = $this->crud_model->select_one("undangan", "undangan_id", $data_tamu_undangan->undangan_id);
                    $tamu_asli = $this->crud_model->select_one("tamu", "tamu_id", $data_tamu_undangan->tamu_id);
                    $this->load->library("whatsapp");
                    $pesan = "ANDA DIMINTA oleh bapak/ibu *" . $tamu_asli->nama_lengkap . "*  untuk menghadiri acara (Terlampir Sebagai Berikut)\n\n";
                    $pesan .= "Assalam alaikum Wr.Wb.\n";
                    $pesan .= "Kepada Yth *" . $data_tamu['nama_lengkap'] . "* \n";
                    $pesan .= "Dengan ini *" . ambil_nama_by_id("user", "nama", "user_id", $undangan->user_id) . "* mengundang Bapak/Ibu untuk menghadiri Acara *" . $undangan->judul . "* yang akan dilaksanakan pada :\n";
                    $pesan .= "Hari : *" . hari($undangan->tanggal) . "*\n";
                    $pesan .= "Tanggal : *" . tgl_indonesia($undangan->tanggal) . "*\n";
                    $pesan .= "Jam : *" . jam($undangan->tanggal . " " . $undangan->jam) . "*\n";
                    $pesan .= "Tempat : *" . $undangan->nama_lokasi . "*\n";
                    $pesan .= "Keterangan : *" . $undangan->keterangan . "*\n";
                    $pesan .= "Untuk Konfirmasi Kehadiran dan data pada Daftar Hadir silahkan klik Link dibawah ini :\n";
                    $pesan .= base_url($tamu_undangan_id) . "\n";
                    $pesan .= "Demikian undangan ini kami sampaikan dan atas perhatiannya kami ucapkan terima kasih.\n";
                    $pesan .= "*TTD*\n";
                    $pesan .= ambil_nama_by_id("user", "nama", "user_id", $undangan->user_id) . "\n\n";
                    $pesan .= "_Harap Balas *Ya* jika link konfirmasi kehadiran tidak bisa di Klik_\n";
                    $pesan .= "Send By Toduwo.id\n";
                    $this->whatsapp->sendMessage($data_tamu['no_telp'], $pesan);
                    // $this->whatsapp->sendImage($data_tamu['no_telp'], "Tunjukkan Code Ini Pada Penerima Tamu", base_url("uploads/qrcode/" . $tamu_undangan_id . ".png"));
                    $ret = [
                        "status" => "1",
                        "message" => "Perwakilan berhasil disimpan",
                    ];
                }
            } else {
                $ret = [
                    "status" => "0",
                    "message" => "Nomor tidak terdaftar di WhatsApp",
                    "error"  => true
                ];
            }
        } else {
            $this->crud_model->update("tamu_undangan", ["dihadiri" => $cek_tamu->tamu_id], "tamu_undangan_id", $tamu_undangan_id);

            $data_tamu_undangan = $this->crud_model->select_one("tamu_undangan", "tamu_undangan_id", $tamu_undangan_id);
            $undangan = $this->crud_model->select_one("undangan", "undangan_id", $data_tamu_undangan->undangan_id);
            $tamu_asli = $this->crud_model->select_one("tamu", "tamu_id", $data_tamu_undangan->tamu_id);
            $tamu_wakili = $this->crud_model->select_one("tamu", "tamu_id", $data_tamu_undangan->dihadiri);
            $this->load->library("whatsapp");
            $pesan = "ANDA DIMINTA oleh bapak/ibu *" . $tamu_asli->nama_lengkap . "*  untuk menghadiri acara (Terlampir Sebagai Berikut)\n\n";
            $pesan .= "Assalam alaikum Wr.Wb.\n";
            $pesan .= "Kepada Yth *" . $tamu_wakili->nama_lengkap . "* \n";
            $pesan .= "Dengan ini *" . ambil_nama_by_id("user", "nama", "user_id", $undangan->user_id) . "* mengundang Bapak/Ibu untuk menghadiri Acara *" . $undangan->judul . "* yang akan dilaksanakan pada :\n";
            $pesan .= "Hari : *" . hari($undangan->tanggal) . "*\n";
            $pesan .= "Tanggal : *" . tgl_indonesia($undangan->tanggal) . "*\n";
            $pesan .= "Jam : *" . jam($undangan->tanggal . " " . $undangan->jam) . "*\n";
            $pesan .= "Tempat : *" . $undangan->nama_lokasi . "*\n";
            $pesan .= "Keterangan : *" . $undangan->keterangan . "*\n";
            $pesan .= "Untuk Konfirmasi Kehadiran dan data pada Daftar Hadir silahkan klik Link dibawah ini :\n";
            $pesan .= base_url($tamu_undangan_id) . "\n";
            $pesan .= "Demikian undangan ini kami sampaikan dan atas perhatiannya kami ucapkan terima kasih.\n";
            $pesan .= "*TTD*\n";
            $pesan .= ambil_nama_by_id("user", "nama", "user_id", $undangan->user_id) . "\n\n";
            $pesan .= "_Harap Balas *Ya* jika link konfirmasi kehadiran tidak bisa di Klik_\n";
            $pesan .= "Send By Toduwo.id\n";
            $this->whatsapp->sendMessage($tamu_wakili->no_telp, $pesan);
            // $this->whatsapp->sendImage($tamu_wakili->no_telp, "Tunjukkan Code Ini Pada Penerima Tamu", base_url("uploads/qrcode/" . $tamu_undangan_id . ".png"));

            $ret = [
                "status" => "1",
                "message" => "Perwakilan berhasil disimpan",
            ];
        }
        echo json_encode($ret);
    }

    // konfirmasi datang orang yang mewakili
    public function datang_wakili()
    {
        $tamu_undangan_id   =   $this->input->post('tamu_undangan_id', true);
        $cek_tamu   =   $this->crud_model->select_one("tamu_undangan", "tamu_undangan_id", $tamu_undangan_id);
        if (empty($cek_tamu)) {
            $ret = [
                "status" => "0",
                "message" => "Tamu tidak ditemukan",
                "error"  => true
            ];
        } else {
            $nama   =   $this->input->post('nama', true);
            $no_telp   =   $this->input->post('no_telp', true);
            $alamat   =   $this->input->post('alamat', true);
            $email   =   $this->input->post('email', true);
            $instansi   =   $this->input->post('instansi', true);
            $jabatan   =   $this->input->post('jabatan', true);
            $this->crud_model->update("tamu_undangan", ["verifikasi_wakili" => "1"], "tamu_undangan_id", $tamu_undangan_id);
            $this->crud_model->update("tamu", [
                "nama_lengkap" => $nama,
                "alamat" => $alamat,
                "email" => $email,
                "instansi" => $instansi,
                "jabatan" => $jabatan
            ], "tamu_id", $cek_tamu->dihadiri);
            // $undangan = $this->crud_model->select_one("undangan", "undangan_id", $cek_tamu->undangan_id);
            $this->load->library("whatsapp");
            $this->whatsapp->sendImage($no_telp, "Tunjukkan Code Ini Pada Penerima Tamu", base_url("uploads/qrcode/" . $tamu_undangan_id . ".png"));
            $ret = [
                "status" => "1",
                "message" => "Terima Kasih"
            ];
        }
        echo json_encode($ret);
    }

    public function registrasi_manual($id = null)
    {
        if ($id === null) {
            $data['title']    =    "Halaman Tidak Ditemukan";
            $this->load->view('e404', $data);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("undangan", ["undangan_id" => $id]);
            if ($cek_data) {
                $data['title']    =    "Halaman Tidak Ditemukan";
                $this->load->view('e404', $data);
            } else {
                if ($this->form_validation->run("registrasi_manual") == false) {
                    $data["undangan"]   =   $this->crud_model->select_one("undangan", "undangan_id", $id);
                    $this->load->view("frontend/registrasi_manual", $data);
                } else {
                    $user_id    =   $this->input->post("user_id", true);
                    $no_telp    =   $this->input->post("no_telp", true);
                    $undangan_id    =   $this->input->post('undangan_id', true);
                    $cek_data   =   $this->crud_model->select_one_where_array("tamu", [
                        "user_id"   =>  $user_id,
                        "no_telp"   =>  $no_telp
                    ]);
                    if (empty($cek_data)) {
                        $data   =   [
                            "tamu_id"   =>  $this->crud_model->cek_id("tamu", "tamu_id"),
                            "nama_lengkap"  =>  $this->input->post('nama', true),
                            "alamat"  =>  $this->input->post('alamat', true),
                            "no_telp"  =>  $this->input->post('no_telp', true),
                            "instansi"  =>  $this->input->post('instansi', true),
                            "jabatan"  =>  $this->input->post('jabatan', true),
                            "user_id"  =>  $user_id,
                        ];

                        $this->load->library("whatsapp");
                        $cek_wa = json_decode($this->whatsapp->checkNumber($no_telp), true);
                        if ($cek_wa["status"] === true) {
                            $data["whatsapp"]   =   "1";
                        } else {
                            $data["whatsapp"]   =   "0";
                        }

                        $data_tamu  =   [
                            "tamu_undangan_id"  =>  generateRandomString("10", TRUE),
                            "undangan_id"   =>  $undangan_id,
                            "tamu_id"   =>  $data["tamu_id"],
                            "lihat_undangan"    =>  "1",
                            "status"    =>  "1",
                            "dihadiri"  =>  $data["tamu_id"],
                        ];
                        $daftar     =   $this->crud_model->insert("tamu", $data);
                        if ($daftar) {
                            $this->crud_model->insert("tamu_undangan", $data_tamu);
                            $notifikasi = array(
                                "status" => "success", "msg" => "Berhasil mendaftar",
                            );
                        } else {
                            $notifikasi = array(
                                "status" => "danger", "msg" => "Gagal mendaftar",
                            );
                        }
                        $this->session->set_flashdata("notifikasi", $notifikasi);
                        redirect("registrasi_selesai/" . $undangan_id);
                    } else {
                        $cek_tamu   =   $this->crud_model->select_one_where_array("tamu_undangan", [
                            "undangan_id"   =>  $undangan_id,
                            "tamu_id"   =>  $cek_data->tamu_id
                        ]);
                        if (empty($cek_tamu)) {
                            $data_tamu  =   [
                                "tamu_undangan_id"  =>  generateRandomString("10", TRUE),
                                "undangan_id"   =>  $undangan_id,
                                "tamu_id"   =>  $cek_data->tamu_id,
                                "lihat_undangan"    =>  "1",
                                "status"    =>  "1",
                                "dihadiri"  =>  $cek_data->tamu_id,
                            ];

                            $daftar     =   $this->crud_model->insert("tamu_undangan", $data_tamu);
                            if ($daftar) {
                                $notifikasi = array(
                                    "status" => "success", "msg" => "Berhasil mendaftar",
                                );
                            } else {
                                $notifikasi = array(
                                    "status" => "danger", "msg" => "Gagal mendaftar",
                                );
                            }
                            $this->session->set_flashdata("notifikasi", $notifikasi);
                            redirect("registrasi_selesai/" . $undangan_id);
                        } else {
                            $data   =   [
                                "dihadiri"  =>  $cek_data->tamu_id,
                                "status"    =>  "1",
                                "lihat_undangan"    =>  "1"
                            ];
                            $daftar     =   $this->crud_model->update("tamu_undangan", $data, "tamu_undangan_id", $cek_tamu->tamu_undangan_id);
                            if ($daftar) {
                                $notifikasi = array(
                                    "status" => "success", "msg" => "Berhasil mendaftar",
                                );
                            } else {
                                $notifikasi = array(
                                    "status" => "danger", "msg" => "Gagal mendaftar",
                                );
                            }
                            $this->session->set_flashdata("notifikasi", $notifikasi);
                            redirect("registrasi_selesai/" . $undangan_id);
                        }
                    }
                }
            }
        }
    }

    public function registrasi_selesai($id = null)
    {
        if ($id === null) {
            $data['title']    =    "Halaman Tidak Ditemukan";
            $this->load->view('e404', $data);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("undangan", ["undangan_id" => $id]);
            if ($cek_data) {
                $data['title']    =    "Halaman Tidak Ditemukan";
                $this->load->view('e404', $data);
            } else {
                $data["undangan"]   =   $this->crud_model->select_one("undangan", "undangan_id", $id);
                $this->load->view("frontend/registrasi_selesai", $data);
            }
        }
    }
}
