<?php

class Tamu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        require_once APPPATH . '/third_party/phpspreadsheet/autoload.php';

        date_default_timezone_set('Asia/Singapore');
        $this->load->model('users_model');
        if (!$this->session->userdata('user')) {
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
            redirect('/login');
        }
    }

    private $base = 'tamu';
    private $folder = 'tamu';

    public function index()
    {

        $data['title'] = "Tamu";
        $data['page'] = $this->folder . "/index";
        $data["data"] = $this->crud_model->select_all_where("tamu", "user_id", user("user_id"));
        $data['base'] = $this->base;
        $this->load->view("backend/main", $data);
    }

    public function tambah()
    {
        if ($this->form_validation->run("tamu") == false) {
            $data['title'] = "Tambah Tamu";
            $data['page'] = $this->folder . "/tambah";
            $data['base'] = $this->base;
            $data['grup'] = $this->crud_model->select_all_where("grup_tamu", "user_id", user("user_id"));
            $this->load->view("backend/main", $data);
        } else {
            $data = [
                "tamu_id" => $this->crud_model->cek_id("tamu", "tamu_id"),
                "nama_lengkap" => strtoupper($this->input->post("nama", true)),
                "alamat" => $this->input->post("alamat", true),
                "email" => $this->input->post("email", true),
                "no_telp" => $this->input->post("no_telp", true),
                "instansi" => $this->input->post("instansi", true),
                "jabatan" => $this->input->post("jabatan", true),
                "user_id" => user("user_id"),
            ];

            $grup   =   $this->input->post('grup');
            $data_grup  =   [];
            if ($grup) {
                $no_grup = 0;
                $grup_tamu_detail_id = $this->crud_model->cek_id("grup_tamu_detail", "grup_tamu_detail_id");
                foreach ($grup as $g) :
                    $data_grup[$no_grup++] = [
                        "grup_tamu_detail_id"   =>  $grup_tamu_detail_id++,
                        "grup_tamu_id" =>  $g,
                        "tamu_id"   =>  $data["tamu_id"]
                    ];
                endforeach;
            }

            $simpan = $this->crud_model->insert("tamu", $data);
            if ($simpan) {
                if ($grup) {
                    $this->crud_model->insert_batch("grup_tamu_detail", $data_grup);
                }
                $notifikasi = array(
                    "status" => "success", "msg" => "Tamu berhasil ditambah",
                );
            } else {
                $notifikasi = array(
                    "status" => "danger", "msg" => "Tamu gagal ditambah",
                );
            }
            $this->session->set_flashdata("notifikasi", $notifikasi);
            redirect($this->base);
        }
    }

    // ubah
    public function ubah($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("tamu", ["tamu_id" => $id]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                if ($this->form_validation->run("tamu") == false) {
                    $data['data'] = $this->crud_model->select_one("tamu", "tamu_id", $id);
                    $data['title'] = "Ubah Tamu";
                    $data['page'] = $this->folder . "/ubah";
                    $data['base'] = $this->base;
                    $this->load->view("backend/main", $data);
                } else {
                    $data = [
                        "nama_lengkap" => strtoupper($this->input->post("nama", true)),
                        "alamat" => $this->input->post("alamat", true),
                        "email" => $this->input->post("email", true),
                        "no_telp" => $this->input->post("no_telp", true),
                        "instansi" => $this->input->post("instansi", true),
                        "jabatan" => $this->input->post("jabatan", true),
                    ];
                    $simpan = $this->crud_model->update("tamu", $data, "tamu_id", $this->input->post("id"));
                    if ($simpan) {
                        $notifikasi = array(
                            "status" => "success", "msg" => "Tamu berhasil diubah",
                        );
                    } else {
                        $notifikasi = array(
                            "status" => "danger", "msg" => "Tamu gagal diubah",
                        );
                    }
                    $this->session->set_flashdata("notifikasi", $notifikasi);
                    redirect($this->base);
                }
            }
        }
    }

    // hapus
    public function hapus($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("tamu", ["tamu_id" => $id]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                $hapus = $this->crud_model->hapus_id("tamu", "tamu_id", $id);
                if ($hapus) {
                    $notifikasi = array(
                        "status" => "success", "msg" => "Tamu berhasil dihapus",
                    );
                } else {
                    $notifikasi = array(
                        "status" => "danger", "msg" => "Tamu gagal diubah",
                    );
                }
                $this->session->set_flashdata("notifikasi", $notifikasi);
                redirect($this->base);
            }
        }
    }

    // cek option bernilai 0
    public function check_default($post_string)
    {
        if ($post_string == '0') {
            $this->form_validation->set_message('check_default', '{field} Belum dipilih');
            return false;
        } else {
            return true;
        }
        //return $post_string == '0' ? FALSE : TRUE;
    }

    // cek nomor hp terdaftar di WA
    public function check_nomor($post_string)
    {
        if ($post_string == "") {
            $this->form_validation->set_message('check_nomor', '{field} Harus diisi');
            return false;
        } else {
            $this->load->library('whatsapp');
            $nomor = json_decode($this->whatsapp->checkNumber($post_string), true);
            if ($nomor["status"] === true) {
                $cek_tamu   =   $this->crud_model->select_one_where_array("tamu", ["no_telp" => $post_string, "user_id" => user("user_id")]);
                if (empty($cek_tamu)) {
                    return true;
                } else {
                    $this->form_validation->set_message('check_nomor', "Nomor telah digunakan oleh $cek_tamu->nama_lengkap");
                    return false;
                }
            } else {
                $this->form_validation->set_message('check_nomor', 'Nomor Tidak terdaftar DI WA');
                return false;
            }
        }
    }

    public function import()
    {
        $this->form_validation->set_rules('fileURL', 'Upload File', 'callback_checkFileValidation');

        if ($this->form_validation->run() == false) {
            $data['title'] = "Import Tamu";
            $data['page'] = $this->folder . "/import";
            $data['base'] = $this->base;
            $this->load->view("backend/main", $data);
        } else {
            $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();

            $spreadsheet = $reader->load($_FILES['fileURL']['tmp_name']);

            $allDataInSheet = $spreadsheet->getActiveSheet()->toArray(null, true, true, true, true, true);
            $arrayCount = count($allDataInSheet);
            $flag = 0;
            $createArray = array('Nama_Lengkap', 'Alamat', 'Email', 'No_Whatsapp', 'Jabatan', 'Instansi');
            $makeArray = array('Nama_Lengkap' => 'Nama_Lengkap', 'Alamat' => 'Alamat', 'Email' => 'Email', 'No_Whatsapp' => 'No_Whatsapp', 'Jabatan' => 'Jabatan', 'Instansi' => 'Instansi');
            $SheetDataKey = array();
            foreach ($allDataInSheet as $dataInSheet) {
                foreach ($dataInSheet as $key => $value) {
                    if (in_array(trim($value), $createArray)) {
                        $value = preg_replace('/\s+/', '', $value);
                        $SheetDataKey[trim($value)] = $key;
                    }
                }
            }

            $dataDiff = array_diff_key($makeArray, $SheetDataKey);
            if (empty($dataDiff)) {
                $flag = 1;
            }
            if ($flag == 1) {
                $fetchData = array();
                for ($i = 2; $i <= $arrayCount; $i++) {
                    $namaLengkap = $SheetDataKey['Nama_Lengkap'];
                    $alamat = $SheetDataKey['Alamat'];
                    $email = $SheetDataKey['Email'];
                    $no_Whatsapp = $SheetDataKey['No_Whatsapp'];
                    $jabatan = $SheetDataKey['Jabatan'];
                    $instansi = $SheetDataKey['Instansi'];


                    $namaLengkap = filter_var(trim($allDataInSheet[$i][$namaLengkap]), FILTER_SANITIZE_STRING);
                    $alamat = filter_var(trim($allDataInSheet[$i][$alamat]), FILTER_SANITIZE_STRING);
                    $email = filter_var(trim($allDataInSheet[$i][$email]), FILTER_SANITIZE_EMAIL);
                    $no_Whatsapp = filter_var(substr_replace($allDataInSheet[$i][$no_Whatsapp], '0', 0, 2), FILTER_SANITIZE_STRING);
                    $jabatan = filter_var(trim($allDataInSheet[$i][$jabatan]), FILTER_SANITIZE_STRING);
                    $instansi = filter_var(trim($allDataInSheet[$i][$instansi]), FILTER_SANITIZE_STRING);

                    $this->load->library("whatsapp");
                    $cek_nomor = json_decode($this->whatsapp->checkNumber($no_Whatsapp), true);
                    if ($cek_nomor["status"] === true) {
                        $no_wa = "1";
                    } else {
                        $no_wa = "0";
                    }
                    // if($cek_nomor)
                    $cek_tamu   =   $this->crud_model->cek_data_existing("tamu", "no_telp", $no_Whatsapp);
                    if ($cek_tamu) {
                        $fetchData[] = array(
                            'nama_lengkap' => strtoupper($namaLengkap),
                            'alamat' => $alamat, 'email' => $email,
                            'no_telp' => $no_Whatsapp, 'whatsapp' => $no_wa, 'instansi' => $instansi,
                            'jabatan' => $jabatan, 'user_id' => user("user_id")
                        );
                    }
                }

                if (count((array) ($fetchData)) > 0) {
                    $data['dataInfo'] = $fetchData;
                    $simpan = $this->crud_model->insert_batch("tamu", $fetchData);
                    if ($simpan) {

                        $notifikasi = array(
                            "status" => "success", "msg" => "Tamu berhasil ditambah",
                        );
                    } else {
                        $notifikasi = array(
                            "status" => "danger", "msg" => "Tamu gagal ditambah",
                        );
                    }
                } else {
                    $notifikasi = array(
                        "status" => "danger", "msg" => "Data yang anda import Kosong",
                    );
                }
            } else {
                $notifikasi = array(
                    "status" => "danger", "msg" => "Data yang anda import tidak sesuai dengan format",
                );
            }
            $this->session->set_flashdata("notifikasi", $notifikasi);
            redirect($this->base);
        }
    }

    // checkFileValidation
    public function checkFileValidation($string)
    {
        $file_mimes = array(
            'text/x-comma-separated-values',
            'text/comma-separated-values',
            'application/octet-stream',
            'application/vnd.ms-excel',
            'application/x-csv',
            'text/x-csv',
            'text/csv',
            'application/csv',
            'application/excel',
            'application/vnd.msexcel',
            'text/plain',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        );
        if (isset($_FILES['fileURL']['name'])) {
            $arr_file = explode('.', $_FILES['fileURL']['name']);
            $extension = end($arr_file);
            if (($extension == 'xlsx' || $extension == 'xls' || $extension == 'csv') && in_array($_FILES['fileURL']['type'], $file_mimes)) {
                return true;
            } else {
                $this->form_validation->set_message('checkFileValidation', 'Please choose correct file.');
                return false;
            }
        } else {
            $this->form_validation->set_message('checkFileValidation', 'Please choose a file.');
            return false;
        }
    }
}
