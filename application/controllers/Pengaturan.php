<?php
class Pengaturan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
		if (!$this->session->userdata('user')) {
			$this->load->helper('url');
			$this->session->set_userdata('last_page', current_url());
			redirect('/login');
		}
	}

	private $base = 'pengaturan';
	private $folder = 'pengaturan';

	// akun
	public function akun()
	{
		$cek_data	=	user("user_id");
		if (!$cek_data) {
			redirect("logout");
		} else {
			if ($this->form_validation->run("password_ubah") == FALSE) {
				$data['data']			=	$this->crud_model->select_one("user", "user_id", user("user_id"));
				$data['title']			=	"Akun Saya";
				$data['page']			=	$this->folder . "/akun";
				$data['base']			=	$this->base;
				$this->load->view("backend/main", $data);
			} else {

				$data	=	[
					"password"	=>	password_hash($this->input->post("password_baru", true), PASSWORD_DEFAULT)
				];

				$simpan = $this->crud_model->update("user", $data, "user_id", user("user_id"));
				if ($simpan) {
					$notifikasi		=	array(
						"status"	=>	"success", "msg"	=>	"Password berhasil diubah"
					);
				} else {
					$notifikasi		=	array(
						"status"	=>	"danger", "msg"	=>	"Password gagal diubah"
					);
				}
				$this->session->set_flashdata("notifikasi", $notifikasi);
				redirect("pengaturan/akun");
			}
		}
	}

	// cek file
	public function check_file($str, $name)
	{
		$allowed_mime_type_arr = array('image/jpeg', 'image/pjpeg', 'image/x-citrix-jpeg', 'image/png', 'image/x-png', 'image/x-citrix-png');
		$mime = get_mime_by_extension($_FILES[$name]['name']);
		if (isset($_FILES[$name]['name']) && $_FILES[$name]['name'] != "") {
			if (in_array($mime, $allowed_mime_type_arr)) {
				if ($_FILES[$name]['size'] > '10485760') {
					$this->form_validation->set_message('check_file', 'Maksimal 10MB');
					return false;
				} else {
					return true;
				}
			} else {
				$this->form_validation->set_message('check_file', 'Pilih file JPG atau PNG');
				return false;
			}
		} else {
			return true;
		}
	}

	// cek password lama
	function passlama_check($post_string)
	{
		if (password_verify($post_string, user("password"))) {
			return TRUE;
		} else {
			$this->form_validation->set_message('passlama_check', '{field} Tidak Sesuai');
			return FALSE;
		}
	}
}
