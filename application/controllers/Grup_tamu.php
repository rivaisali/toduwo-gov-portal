<?php

class Grup_tamu extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        require_once APPPATH . '/third_party/phpspreadsheet/autoload.php';

        date_default_timezone_set('Asia/Singapore');
        $this->load->model('users_model');
        if (!$this->session->userdata('user')) {
            $this->load->helper('url');
            $this->session->set_userdata('last_page', current_url());
            redirect('/login');
        }
    }

    private $base = 'grup_tamu';
    private $folder = 'grup_tamu';

    public function index($id = null)
    {
        if ($id == null) {
            $data['title'] = "Grup Tamu";
            $data['page'] = $this->folder . "/index";
            $data["data"] = $this->crud_model->select_all_where("grup_tamu", "user_id", user("user_id"));
            $data['base'] = $this->base;
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("grup_tamu", ["grup_tamu_id" => $id, "user_id" => user("user_id")]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                $grup_tamu  =   $this->crud_model->select_one("grup_tamu", "grup_tamu_id", $id);
                $this->load->model("join_model");
                $data['title'] = "Detail Grup Tamu : " . $grup_tamu->nama_grup_tamu;
                $data['page'] = $this->folder . "/detail";
                $data["data"] = $grup_tamu;
                $data['tamu'] = $this->join_model->dua_tabel_where("grup_tamu_detail", "tamu", "tamu_id", "grup_tamu_id", $id);
                $data['base'] = $this->base;
            }
        }
        $this->load->view("backend/main", $data);
    }

    public function tambah()
    {
        if ($this->form_validation->run("grup_tamu") == false) {
            $data['title'] = "Tambah Grup Tamu";
            $data['page'] = $this->folder . "/tambah";
            $data['base'] = $this->base;
            $this->load->view("backend/main", $data);
        } else {
            $data = [
                "grup_tamu_id" => $this->crud_model->cek_id("grup_tamu", "grup_tamu_id"),
                "nama_grup_tamu" => strtoupper($this->input->post("nama", true)),
                "deskripsi" =>  $this->input->post("deskripsi", true),
                "user_id" => user("user_id")
            ];
            $simpan = $this->crud_model->insert("grup_tamu", $data);
            if ($simpan) {
                $notifikasi = array(
                    "status" => "success", "msg" => "Grup Tamu berhasil ditambah",
                );
            } else {
                $notifikasi = array(
                    "status" => "danger", "msg" => "Grup Tamu gagal ditambah",
                );
            }
            $this->session->set_flashdata("notifikasi", $notifikasi);
            redirect($this->base);
        }
    }

    // ubah
    public function ubah($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("grup_tamu", ["grup_tamu_id" => $id, "user_id" => user("user_id")]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                if ($this->form_validation->run("grup_tamu") == false) {
                    $data['data'] = $this->crud_model->select_one("grup_tamu", "grup_tamu_id", $id);
                    $data['title'] = "Ubah Grup Tamu";
                    $data['page'] = $this->folder . "/ubah";
                    $data['base'] = $this->base;
                    $this->load->view("backend/main", $data);
                } else {
                    $data = [
                        "nama_grup_tamu" => strtoupper($this->input->post("nama", true)),
                        "deskripsi" => $this->input->post("deskripsi", true),
                    ];
                    $simpan = $this->crud_model->update("grup_tamu", $data, "grup_tamu_id", $this->input->post("id"));
                    if ($simpan) {
                        $notifikasi = array(
                            "status" => "success", "msg" => "Grup Tamu berhasil diubah",
                        );
                    } else {
                        $notifikasi = array(
                            "status" => "danger", "msg" => "Grup Tamu gagal diubah",
                        );
                    }
                    $this->session->set_flashdata("notifikasi", $notifikasi);
                    redirect($this->base);
                }
            }
        }
    }

    // hapus
    public function hapus($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("grup_tamu", ["grup_tamu_id" => $id, "user_id" => user("user_id")]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                $hapus = $this->crud_model->hapus_id("grup_tamu", "grup_tamu_id", $id);
                if ($hapus) {
                    $notifikasi = array(
                        "status" => "success", "msg" => "Grup Tamu berhasil dihapus",
                    );
                } else {
                    $notifikasi = array(
                        "status" => "danger", "msg" => "Grup Tamu gagal diubah",
                    );
                }
                $this->session->set_flashdata("notifikasi", $notifikasi);
                redirect($this->base);
            }
        }
    }

    // manajemen tamu
    public function tambah_tamu($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("grup_tamu", ["grup_tamu_id" => $id, "user_id" => user("user_id")]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                if ($this->form_validation->run("grup_tamu_detail") == false) {
                    $grup_tamu  =   $this->crud_model->select_one("grup_tamu", "grup_tamu_id", $id);
                    $data['data'] = $grup_tamu;
                    $data['title'] = "Tambah Tamu Ke Dalam Grup : " . $grup_tamu->nama_grup_tamu;
                    $data['page'] = $this->folder . "/tambah_tamu";
                    $user_id = user("user_id");
                    $query = "SELECT * FROM tamu WHERE user_id = ' $user_id ' and tamu_id NOT IN(SELECT tamu_id  FROM grup_tamu_detail where grup_tamu_id = '$id')";
                    $data['tamu'] = $this->crud_model->select_custom($query);
                    $data['base'] = $this->base;
                    $this->load->view("backend/main", $data);
                } else {
                    $tamu = $this->input->post('tamu', true);
                    $ind = 0;
                    $grup_tamu_detail_id = $this->crud_model->cek_id("grup_tamu_detail", "grup_tamu_detail_id");
                    foreach ($tamu as $t) :
                        $data[$ind++] = [
                            "grup_tamu_detail_id" => $grup_tamu_detail_id++,
                            "grup_tamu_id" => $this->input->post('grup_tamu_id', true),
                            "tamu_id" => $t,
                        ];
                    endforeach;

                    $simpan = $this->crud_model->insert_batch("grup_tamu_detail", $data);
                    if ($simpan) {
                        $notifikasi = array(
                            "status" => "success", "msg" => "Tamu berhasil ditambah ke grup",
                        );
                    } else {
                        $notifikasi = array(
                            "status" => "danger", "msg" => "Tamu gagal ditambah ke grup",
                        );
                    }
                    $this->session->set_flashdata("notifikasi", $notifikasi);
                    redirect($this->base . "/" . $this->input->post('grup_tamu_id', true));
                }
            }
        }
    }

    // hapus tamu
    public function hapus_tamu($grup_tamu = null, $id = null)
    {
        if ($id === null || $grup_tamu === null) {
            redirect($this->base);
        } else {
            $this->load->model("join_model");
            $cek_data = $this->join_model->dua_tabel_where_array_row("grup_tamu", "grup_tamu_detail", "grup_tamu_id", [
                "grup_tamu.grup_tamu_id" => $grup_tamu,
                "grup_tamu_detail_id" => $id
            ]);
            if (empty($cek_data)) {
                redirect($this->base);
            } else {
                $hapus = $this->crud_model->hapus_id("grup_tamu_detail", "grup_tamu_detail_id", $id);
                if ($hapus) {
                    $notifikasi = array(
                        "status" => "success", "msg" => "Tamu berhasil dihapus dari grup",
                    );
                } else {
                    $notifikasi = array(
                        "status" => "danger", "msg" => "Tamu gagal dihapus dari grup",
                    );
                }
                $this->session->set_flashdata("notifikasi", $notifikasi);
                redirect($this->base . "/" . $cek_data->grup_tamu_id);
            }
        }
    }

    // fungsi cek tamu
    public function tamu_check()
    {
        if (!$this->input->post('tamu[]')) {
            $this->form_validation->set_message('tamu_check', 'Tamu belum dipilih');
            return false;
        } else {
            return true;
        }
    }
}
