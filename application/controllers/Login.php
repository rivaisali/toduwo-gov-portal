<?php
class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
        $this->load->model('users_model');
    }

    public $base = 'frontend';
    public $konten = '/frontend';

    public function index()
    {
        if ($this->session->userdata('user')) {
            redirect('backend');
        } else {
            $data['title'] = "Login";
            $this->load->view("frontend/login", $data);
        }

    }

    public function do_login()
    {
        $output = array('error' => false);

        $email = $this->input->post('email', true);
        $cek_email = $this->crud_model->select_one("user", "email", $email);
        if (!empty($cek_email)) {
            $password = password_verify($this->input->post("password", true), $cek_email->password);
            if ($password) {
                $data = $this->users_model->login($email);
                if ($data['status'] == "0") {
                    $output['error'] = true;
                    $output['message'] = 'Akun anda di Non-aktifkan';
                } else {
                    $update = array(
                        "last_login" => date("Y-m-d H:i:s"),
                    );
                    $this->crud_model->update("user", $update, "user_id", $data['user_id']);
                    $this->session->set_userdata('user', $data);
                    $output['message'] = 'Logging in. Please wait...';
                    // $output['data'] = $data;
                }
            } else {
                $output['error'] = true;
                $output['message'] = 'Password Salah';
            }
        } else {
            $output['error'] = true;
            $output['message'] = 'Email belum terdaftar';
        }

        echo json_encode($output);
    }

    public function logout()
    {
        $notifikasi = array(
            "status" => "info", "msg" => "Selamat Tinggal. Anda Berhasil Keluar",
        );

        $this->session->set_flashdata("notifikasi", $notifikasi);
        // $this->session->unset_userdata('user');
        $this->session->sess_destroy();
        redirect();
    }
}
