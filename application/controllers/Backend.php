<?php
class Backend extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Singapore');
		$this->load->model('users_model');
		if (!$this->session->userdata('user')) {
			$this->load->helper('url');
			$this->session->set_userdata('last_page', current_url());
			redirect('/');
		}
	}

	public function index()
	{
		$data['title']			=	"Beranda";
		$data['page']			=	"/beranda";
		if (user("level") == "admin") {
			$data['pengguna']	=	$this->crud_model->select_all_where_array_num_row("user", ["level" => "user"]);
		}
		$data['tamu']	=	$this->crud_model->select_all_where_array_num_row("tamu", ["user_id" => user("user_id")]);
		$data['undangan']	=	$this->crud_model->select_all_where_array_num_row("undangan", ["user_id" => user("user_id")]);
		$this->load->view("backend/main", $data);
	}

	public function create_undangan()
	{
		// print_r($this->input->post());
		$this->form_validation->set_rules('judul', 'Judul Acara', 'trim|required');
		$this->form_validation->set_rules('subdomain', 'Subdomain', 'trim|required|callback_subdomain_check');
		$this->form_validation->set_rules('tgl', 'Tanggal dan Jam', 'trim|required|callback_tgl_check');
		$this->form_validation->set_rules('lokasi', 'Lokasi Acara', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Alamat Lokasi', 'trim|required');
		$this->form_validation->set_rules('nama_pp', 'Nama Pengantin Pria', 'trim|required');
		$this->form_validation->set_rules('deskripsi_pp', 'Tentang Pengantin Pria', 'trim|required');
		$this->form_validation->set_rules('foto_pp', 'Foto Pengantin Pria', 'trim|callback_foto_check');
		$this->form_validation->set_rules('nama_pw', 'Nama Pengantin Wanita', 'trim|required');
		$this->form_validation->set_rules('deskripsi_pw', 'Tentang Pengantin Wanita', 'trim|required');
		$this->form_validation->set_rules('foto_pw', 'Foto Pengantin Wanita', 'trim|callback_foto_check');
		// $this->form_validation->set_rules('', 'Tentang Pengantin Wanita', 'trim|required');
		$this->form_validation->set_error_delimiters('<div class="invalid-feedback">', '</div>');

		if ($this->form_validation->run() == FALSE) {
			$user					=	$this->crud_model->select_one("user", "user_id", user("user_id"));
			$data['title']			=	"Buat Undangan";
			$data['desain']			=	$this->crud_model->select_like("desain", "paket", $user->paket_id);
			$this->load->view("backend/undangan", $data);
		} else {
			$data_undangan = [
				"undangan_id" 				=>	$this->crud_model->cek_id("undangan", "undangan_id"),
				"user_id" 					=>	user("user_id"),
				"desain_id" 				=>	$this->input->post("desain", true),
				"judul" 					=>	$this->input->post("judul", true),
				"nama_domain"				=>	$this->input->post("subdomain", true),
				"nama_pengantin_pria"		=>	$this->input->post("nama_pp", true),
				"tentang_pengantin_pria"	=>	$this->input->post("deskripsi_pp", true),
				"nama_pengantin_wanita"		=>	$this->input->post("nama_pw", true),
				"tentang_pengantin_wanita"	=>	$this->input->post("deskripsi_pw", true),
			];

			$config['upload_path']     = './uploads/pengantin/';
			$config['allowed_types']   = 'jpg|png';
			$config['max_size']       	= 2000;
			$config['max_filename'] 	= '255';
			$config['encrypt_name'] 	= TRUE;

			$this->load->library('upload', $config);
			if ($this->upload->do_upload('foto_pp')) {
				$data_file = $this->upload->data();
				$data_undangan['foto_pengantin_pria']	=	$data_file['file_name'];
			}
			if ($this->upload->do_upload('foto_pw')) {
				$data_file = $this->upload->data();
				$data_undangan['foto_pengantin_wanita']	=	$data_file['file_name'];
			}

			$pisah_tgl	=	explode(" - ", $this->input->post("tgl"));

			$data_acara	= [
				"acara_id"		=>	$this->crud_model->cek_id("acara", "acara_id"),
				"undangan_id"	=>	$data_undangan["undangan_id"],
				"user_id"		=>	user("user_id"),
				"tanggal"		=>	$pisah_tgl[0],
				"jam"			=>	$pisah_tgl[1],
				"nama_lokasi"	=>	$this->input->post("lokasi", true),
				"alamat_lokasi"	=>	$this->input->post("alamat", true)
			];
			// print_r($data_acara);
			$simpan = $this->crud_model->insert("undangan", $data_undangan);
			if ($simpan) {
				$this->crud_model->insert("acara", $data_acara);
				$notifikasi		=	array(
					"status"	=>	"success", "msg"	=>	"Undangan anda berhasil dibuat. Subdomain anda sedang kami kerjakan."
				);
			} else {
				$notifikasi		=	array(
					"status"	=>	"danger", "msg"	=>	"Undangan gagal dibuat."
				);
			}
			$this->session->set_flashdata("notifikasi", $notifikasi);
			redirect("backend");
		}
	}

	function subdomain_check($post_string)
	{
		// if ($post_string == '0') {
		// 	$this->form_validation->set_message('subdomain_check', '{field} Belum dipilih');
		// 	return FALSE;
		// } else {
		return TRUE;
		// }
	}

	function tgl_check($post_string)
	{
		// if ($post_string == '0') {
		// 	$this->form_validation->set_message('subdomain_check', '{field} Belum dipilih');
		// 	return FALSE;
		// } else {
		return TRUE;
		// }
	}

	function foto_check($post_string)
	{
		// if ($post_string == '0') {
		// 	$this->form_validation->set_message('subdomain_check', '{field} Belum dipilih');
		// 	return FALSE;
		// } else {
		return TRUE;
		// }
	}
}
