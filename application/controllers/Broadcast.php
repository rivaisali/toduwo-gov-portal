<?php

use function GuzzleHttp\json_decode;

class Broadcast extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
        $this->load->model('users_model');
        if (!$this->session->userdata('user') && user("level") != "admin") {
            redirect('/logout');
        }
    }

    private $base = 'broadcast';
    private $folder = 'broadcast';

    public function index()
    {
        $data['title']            =    "Broadcast";
        $data['page']            =    $this->folder . "/index";
        $data["data"]            =    $this->crud_model->select_all_order("broadcast", "waktu_kirim", "DESC");
        $data['base']            =    $this->base;
        $this->load->view("backend/main", $data);
    }

    public function detail($id = null)
    {
        if ($id === null) {
            redirect($this->base);
        } else {
            $cek_data = $this->crud_model->cek_data_where_array("broadcast", ["broadcast_id" => $id]);
            if ($cek_data) {
                redirect($this->base);
            } else {
                $data['title']           =    "Broadcast";
                $data['page']            =    $this->folder . "/detail";
                $data["data"]            =    $this->crud_model->select_one("broadcast", "broadcast_id", $id);
                $data['base']            =    $this->base;
                $this->load->view("backend/main", $data);
            }
        }
    }

    // tambah pesan
    public function tambah_pesan()
    {
        if ($this->form_validation->run("pesan_broadcast") == FALSE) {
            $data['title']           =  "Tambah Pesan Broadcast";
            $data['page']            =  $this->folder . "/tambah_pesan";
            $data['base']            =  $this->base;
            $this->load->view("backend/main", $data);
        } else {
            $this->load->library("whatsapp");
            $pesan = str_replace('\n', "\n", $this->input->post('isi_pesan', true));
            $this->session->set_userdata("judul_broadcast", $this->input->post("judul"));
            $this->session->set_userdata("pesan_broadcast", $pesan);
            $this->session->set_userdata("caption_gambar", $this->input->post("caption"));

            $config['upload_path'] = './uploads/broadcast/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['max_size'] = 2000;
            $config['max_filename'] = '255';
            $config['encrypt_name'] = true;

            $this->load->library('upload', $config);
            if ($this->upload->do_upload('gambar')) {
                if ($this->session->userdata("gambar_broadcast")) {
                    unlink('./uploads/broadcast/' . $this->session->userdata("gambar_broadcast"));
                }
                $data_file = $this->upload->data();
                $this->session->set_userdata("gambar_broadcast", $data_file['file_name']);
                // $data['file_undangan'] = $data_file['file_name'];
            }

            redirect($this->base . "/tambah_penerima");
        }
    }

    // tambah penerima pesan
    public function tambah_penerima()
    {
        if ($this->form_validation->run("penerima_broadcast") == FALSE) {
            $data['title']           =  "Tambah Penerima Pesan Broadcast";
            $data['page']            =  $this->folder . "/tambah_penerima";
            $query  =   "SELECT tamu.tamu_id, tamu.whatsapp, tamu.nama_lengkap, tamu.alamat, tamu.no_telp, tamu.instansi, tamu.jabatan, user.nama FROM tamu JOIN user ON tamu.user_id = user.user_id GROUP BY tamu.no_telp";
            $data["data"] = $this->crud_model->select_custom($query);
            $data['base']            =  $this->base;
            $this->load->view("backend/main", $data);
        } else {
            $penerima   =   $this->input->post("tamu[]");
            $judul      =   $this->session->userdata("judul_broadcast");
            $pesan      =   $this->session->userdata("pesan_broadcast");
            $data       =   [
                "broadcast_id"  =>  $this->crud_model->cek_id("broadcast", "broadcast_id"),
                "judul" =>  $judul,
                "pesan" =>  $pesan,
                // "penerima"  =>  []
            ];
            $penerima_pesan =   [];
            $this->load->library("whatsapp");
            foreach ($penerima as $ind => $val) {
                if ($this->session->userdata("gambar_broadcast")) {
                    $gambar =   base_url("uploads/broadcast/" . $this->session->userdata("gambar_broadcast"));
                    $this->whatsapp->sendImage($val, $this->session->userdata("caption_gambar"), $gambar);
                }
                $kirim_pesan = $this->whatsapp->sendMessage($val, $pesan);
                $result =   json_decode($kirim_pesan, true);
                if ($result["status"] == "1") {
                    array_push($penerima_pesan, ["tamu_id" => $ind]);
                }
            }
            $data["penerima"]   =   json_encode($penerima_pesan);
            if ($this->session->userdata("gambar_broadcast")) {
                $data["gambar"] =   $this->session->userdata("gambar_broadcast");
                $data["caption"]    =   $this->session->userdata("caption_gambar");
            }
            $this->crud_model->insert("broadcast", $data);
            $notifikasi        =    array(
                "status"    =>    "success", "msg"    =>    "Pesan berhasil dibroadcast"
            );
            $this->session->set_flashdata("notifikasi", $notifikasi);
            $this->session->unset_userdata("judul_broadcast");
            $this->session->unset_userdata("pesan_broadcast");
            $this->session->unset_userdata("gambar_broadcast");
            $this->session->unset_userdata("caption_gambar");
            redirect($this->base);
        }
    }

    // fungsi cek tamu
    public function tamu_check()
    {
        if (!$this->input->post('tamu[]')) {
            $this->form_validation->set_message('tamu_check', 'Penerima belum dipilih');
            return false;
        } else {
            return true;
        }
    }

    // cek file
    public function check_file($str, $name)
    {
        $allowed_mime_type_arr = array('image/jpg', 'image/jpeg', 'image/png');
        if (isset($_FILES[$name]['name']) && $_FILES[$name]['name'] != "") {
            $mime = get_mime_by_extension($_FILES[$name]['name']);
            if (in_array($mime, $allowed_mime_type_arr)) {
                // if ($_FILES[$name]['size'] > '10485760') {
                if ($_FILES[$name]['size'] > '2097152') {
                    $this->form_validation->set_message('check_file', 'Maksimal 2MB');
                    return false;
                } else {
                    return true;
                }
            } else {
                $this->form_validation->set_message('check_file', 'Gambar harus berformat JPG atau PNG');
                return false;
            }
        } else {
            return true;
        }
    }
}
