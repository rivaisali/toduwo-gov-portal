<?php
class Frontend extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Singapore');
        $this->load->model('users_model');
        // if ($this->session->userdata('user')) {
        //     $this->load->helper('url');
        //     $this->session->set_userdata('last_page', current_url());
        //     redirect('/login');
        // }
        if ($this->session->userdata('user')) {
            redirect('backend');
        }
    }

    public function index()
    {
        $data['title'] = "Beranda";
        $data['page'] = "/beranda";
        $waktu = date('Y-m-d');
        $ip = $_SERVER['REMOTE_ADDR'];
        $cek = $this->crud_model->select_one_where_array("visitors", array("ip" => $ip, "date" => $waktu));
        if (empty($cek)) {
            $this->crud_model->insert("visitors", array("date" => $waktu, "ip" => $ip, "views" => "1"));
        }

        $hari_ini = $this->crud_model->select_sum("visitors", "views", array("date" => date("Y-m-d")));
        $total = $this->crud_model->select_sum("visitors", "views", array("ip <>" => ""));
        $data['kunjungan'] = array("hari_ini" => $hari_ini, "total" => $total);

        $this->load->view("frontend/main", $data);
    }
}
